<head>
  <meta charset="utf-8">
  <!-- <link rel="stylesheet" href="assets/css/responsive-nav.css"> -->

  <meta name="viewport" content="width=device-width, initial-scale=1">
  <script src="assets/js/responsive-nav.js"></script>
  <link href="/vendor/components/bootstrap/css/bootstrap.css" rel="stylesheet" media="screen" type="text/css">
  <script src="/vendor/components/jquery/jquery.min.js" type="application/javascript"></script>
  <script src="/vendor/components/bootstrap/js/bootstrap.min.js" type="application/javascript"></script>
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/fork-awesome@1.1.7/css/fork-awesome.min.css" integrity="sha256-gsmEoJAws/Kd3CjuOQzLie5Q3yshhvmo7YNtBG7aaEY=" crossorigin="anonymous" type="text/css">
  <link rel="stylesheet" type="text/css" media="all" href="assets/css/styles.css" />
  <!--[if IE]>
	<link rel="stylesheet" type="text/css" href="assets/css/ie-styles.css" />
  <![endif]-->


  <meta property="og:title" content="Oceans Past Initiative"/>
  <meta property="og:description" content="The OPI is a global research network for marine historical research. Our goal is to enhance knowledge of how the diversity, distribution and abundance of marine life in the oceans has changed to better indicate future changes."/>
  <meta property="og:type" content="article"/>
  <meta property="og:image" content="assets/img/OPI.svg"/>

  <style>
  body {
   margin: 10px;
   overflow-x: hidden;
   position: relative;
  }
 </style>
  <title>OPI</title>
</head>
