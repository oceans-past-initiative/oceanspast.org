<div class="agenda">
  <div class="project article">
    <div id="">
      <p>We aspire to facilitate networking between practioners in the fields of marine envrionmental history / historical marine ecology. If you would like to have your research project listed here, please submit a website link to Bo Poulsen or Cristina Brito</p>
    </div>
    <div id="ICES">
      <h2><a href="#">ICES WGHIST</a></h2>
      <p>The Oceans Past Initiative is closely linked with the expert working group, WGHIST of the International Council for the Exploration of the Sea (ICES): <a href="http://www.ices.dk/community/groups/Pages/WGHIST.aspx">The ICES Working Group on the History of Fish and Fisheries (WGHIST)</a> brings together fisheries scientists, historians, and marine biologists working on multidecadal to centennial changes in the marine environment.</p>
      <p>WGHIST aims to improve the understanding of the long-term dynamics of fish populations, fishing fleets, and catching technologies. The results are used for setting baselines for management, restoration, and conservation of marine resources and ecosystems.</p>
    </div>
    <div id="EU-COST">
      <h2><a href="#">EU COST Action Network</a></h2>
      <p><a href="http://www.tcd.ie/history/opp/index.php">The Oceans Past Platform (EU COST Action IS1403)</a> was launched earlier 2015: The Action calls on historians, archaeologists and social scientists as well as colleagues from the marine sciences to engage in dialogue and collaboration with ocean and coastal managers. OPP will develop historical descriptors and indicators for marine and coastal management.</p>

      <img src="assets/img/opp.jpg" width="100%" alt="Oceans Past Platform triptique" />

      <p><a href="http://oceanspast.org/OPI/OPPtest1.html">The Action, Oceans Past Platform (OPP)</a>, aims to measure and understand the significance and value to European societies of living marine resource extraction and production to help shape the future of coasts and oceans. The Integrative Platform will lower the barriers between human, social and natural sciences; multiply the learning capacity of research environments; and enable knowledge transfer and co-production among researchers and other societal actors, specifically by integrating historical findings of scale and intensity of resource use into management and policy frameworks.</p>
      <p>The oceans offer rich resources for feeding a hungry world. However, the sea is an alien space in a sense that the land is not. Fishing requires skills that must be learnt, it presupposes culinary preferences, technical ability, knowledge of target species, and a backdrop of material and intangible culture. OPP asks when, how and with what socio-economic, political, cultural and ecological implications humans have impacted marine life, primarily in European seas in the last two millennia.</p>
    </div>
    <div id="publications">
      <h2><a href="#">Publications</a></h2>
      <a href="publications.php">→ Fully detailed OPI publications from 2010 to 2017</a>
    </div>
    <div id="OPP">
      <h2><a href="#">Oceans Past Platform</a></h2>
    </div>
    <div id="HMAP-db">
      <h2><a href="hmap_db.php">HMAP databases</a></h2>
        <a href="hmap_db.php">History of Marine Animal Populations databases and datasets</a>
    </div>
  </div>
</div>
