<html>
<title>OPI VII conference</title>
<meta property="og:title" content="OPI VII conference"/>

  <?php
  if (file_exists('local.txt')) {
      //don't load admin headers
  } else {
      // we are in production server
      include "login/misc/pagehead.php";
  } ?>
  <?php include 'header.php';?>
<body>
  <?php include 'nav.php';?>

<div id="container">
    <main>
      <aside class="left">
        <?php include 'conferences_left.php';?>
      </aside>
      <aside class="right">
        <div class="agenda">
          <div class="project article">
            <div id="about">

              <h1>Oceans Past VIII Conference</h1>
				<h2>The Next Oceans Past Conference will take place October 2020</h2>
				<h3>Bruges, Belgium</h3>
				
				<h2>Updates Forthcoming</h2>

            </div>
          </div>
        </div>
      </aside>

    </main>

  </div>
  <?php include 'footer.php';?>
</body>
