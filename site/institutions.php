<html>
<title>OPI.org</title>
<?php
if (file_exists('local.txt')) {
    //don't load admin headers
} else {
    // we are in production server
    include "login/misc/pagehead.php";
} ?>
<?php include 'header.php';?>
<body>
  <?php include 'nav.php';?>

<div id="container">
    <main>
      <aside class="left">
        <?php include 'about_left.php';?>
      </aside>
      <aside class="right">
        <div class="agenda">
          <div class="project article">
            <div id="steering committee">
              <h1>Institutional Members of the OPI</h1>
              <p>The Oceans Past Initiative is a non-profit organisation. For more information on how your institution may join the OPI, please email info@oceanspast.org. Currently, the OPI operates under the auspices of several institutional members including, but not limited to: </p>
              <hr>
              <a href="https://www.tcd.ie/tceh/">
                <figure>
                  <img class="in-body-half" src="assets/img/logos_OPI_Institutions/Trinity.jpg">
                  <figcaption>Trinity Centre for Environmental Humanities</figcaption>
                </figure>
              </a>
              <hr>
				      <a href="https://www.niwa.co.nz/">
                <figure>
                <img class="in-body-half" src="assets/img/logos_OPI_Institutions/niwa.png">
                <figcaption>National Institute of Atmospheric and Water Research (NIWA), New Zealand</figcaption>
              </figure>
              </a>
              <hr>
				      <a href="http://www.cham.fcsh.unl.pt/">
                <figure>
                  <img class="in-body-half" src="assets/img/logos_OPI_Institutions/cham.png">
                  <figcaption>CHAM (NOVA FCSH—UAc, Lisbon, Portugal) </figcaption>
                </figure>
              </a>
              <hr>
				      <a href="https://www.awi.de/">
                <figure>
                  <img class="in-body-half" src="assets/img/logos_OPI_Institutions/AWI.jpg">
                  <figcaption>Alfred Wegener Institute Helmholtz Center for Polar and Marine Research, Bremerhaven (Germany) </figcaption>
                </figure>
              </a>
              <hr>
				      <a href="http://www.exeter.ac.uk">
                <figure>
                  <img class="in-body-half" src="assets/img/logos_OPI_Institutions/Exeter.png">
                  <figcaption>Centre for Ecology and Conservation, University of Exeter Cornwall </figcaption>
                </figure>
              </a>
              <hr>
              <a href="https://www.washington.edu/">
                <figure>
                  <img class="in-body-half" src="assets/img/logos_OPI_Institutions/Washington.png">
                  <figcaption>University of Washington</figcaption>
                </figure>
              </a>
              <hr>
              <a href="https://www.obis.org/">
                <figure>
                  <img class="in-body-half" src="assets/img/logos_OPI_Institutions/obis.png">
                  <figcaption>The OPI is an OBIS Node<br>Ocean Biogeographic Information System</figcaption>
                </figure>
              </a>
            </div>
          </div>
        </div>
      </aside>
    </main>

  </div>
  <?php include 'footer.php';?>
</body>
</html>
