<html>
<title>OPI.org</title>
<?php
if (file_exists('local.txt')) {
    //don't load admin headers
} else {
    // we are in production server
    include "login/misc/pagehead.php";
} ?>
<?php include 'header.php';?>
<body>
  <?php include 'nav.php';?>

<div id="container">
    <main>

      <aside class="left">
        <?php include 'homepage_left.php';?>
      </aside>
      <aside class="right">
        <?php include 'homepage_projects.php';?>
        <div id="detail-container">
        <?php include 'homepage_agenda.php';?>
        </div>
      </aside>

    </main>

  </div>
  <?php include 'footer.php';?>
</body>
</html>
