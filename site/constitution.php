<html>
<title>OPI Constitution</title>
<meta property="og:title" content="OPI consitution"/>
<?php
if (file_exists('local.txt')) {
    //don't load admin headers
} else {
    // we are in production server
    include "login/misc/pagehead.php";
} ?>
  <?php include 'header.php';?>
<body>
  <?php include 'nav.php';?>

<div id="container">
    <main>
      <aside class="left">
        <?php include 'conferences_left.php';?>
      </aside>
      <aside class="right">
        <div class="agenda">
          <div class="project article">
            <div id="constitution">
              <h1><b>Constitution of the  Oceans Past Initiative</b></h1>
              <h3>1. NAME</h3>
              <p> The name of the organisation shall be Oceans Past Initiative  (OPI).</p>
              <h3>2. AIM </h3>
              <p>The aim of OPI shall be to promote research in and outreach  of the history of human/marine environments through time at all pertinent  levels and through all relevant actions.</p>
              <h3>3. POWERS</h3>
              <p>To further these aims the Governing Board shall have power  to:</p>
              <ol type="I">
                <ol type="I">
                  <li>Obtain, collect and receive money or funds by  way of contributions, donations, grants and any other lawful method towards the  aims of the Group.</li>
                  <li> Promote and organise research, especially by organising  the Oceans Past conference at regular intervals (every two or three years).</li>
                  <li> Do all such lawful things as will further the  aims of the Group.</li>
                </ol>
              </ol>

              <h3>4. MEMBERSHIP</h3>
              <ol type="a">
                <li>Membership  consists of two classes: individual membership (Class A) and institutional  membership (Class B).</li>
                <ol type = "I">
                  <li>Voting membership (Class A) shall be open to any  individual who has paid annual dues. </li>
                  <li>Voting membership (Class B) shall be open to any  institution which has paid annual dues.</li>
                </ol>
                <li>Members must be present at the General Assembly  to cast a vote.</li>
                <li>Members of Class A and B vote together at the General  Assembly. However, members of Class B have the option to elect a non-voting  member of the Board (cf. 5c below). </li>
                <li>The General Assembly shall have the power to  approve or reject applications for membership and to terminate the membership  of any member provided that the member shall have the right to be heard by the General  Assembly before a final decision is made.</li>
                <li>OPI  will adhere to the Principles of Data Protection, as detailed in the General Data Protection Regulation&nbsp;(EU) 2016/679 of the European Union. This means personal  data is safeguarded and cannot be used without the member&rsquo;s consent. </li>
                <li>Membership dues paid after 1 October will cover  the next calendar year. </li>
              </ol>

              <h3>5. MANAGEMENT</h3>
              <ol type="a">
                <li>A Governing Board elected at the General  Assembly shall manage OPI. Members of the Board may be re-elected twice. </li>
                <li>The Board shall consist of a President,  Secretary, Treasurer, and two other voting members.</li>
                <li>Class B members have the option to elect a  non-voting member of the Board.</li>
                <li>If the Board finds it helpful to its operation,  the Board may co-opt up to a further three voting members who shall resign at  the next General Assembly. This  process may be used to fill places which were not filled at an election, fill  vacancies which arise between elections, and bring people with specific skills  or experience onto the committee. The duration of any co-opted membership ends  at the next General Assembly.</li>
                <li>The Board may meet physically but will conduct most  business electronically and meet by virtual means at least six times each year.</li>
                <li>At least three Board members must be present at  a Board meeting to be able to make decisions.</li>
                <li>A record of all decisions by the Board shall be  kept by the Secretary and made available online.</li>
                <li>An Auditor, not member of the Governing Board,  shall be elected by the General Assembly to audit the OPI accounts. The Auditor  may be re-elected once.</li>
              </ol>

              <h3>6. GENERAL ASSEMBLY</h3>
              <ol type="a">
                <li>A General Assembly shall be held in connection  with the OPI conference. A pre-invitation issued by the Secretary shall contain  the Agenda as proposed by the Board and is sent to each member of OPI at least sixty  days in advance of the date of the General Assembly. Members can submit additional  items up to thirty days before the Assembly</li>
                <li>Notices of the General Assembly shall be  published three weeks beforehand by the Secretary. The treasurer must make a  report on the financial position for the previous year available three weeks  before the General Assembly.</li>
                <li>A Special General Assembly may be called at any  time at the request of the Board, or not less than one quarter of the  membership. A notice explaining the place, date, time and reason shall be sent  to all members three weeks beforehand.</li>
                <li>Proposals to change the constitution must be  given in writing to the Secretary at least 28 days before a General Assembly  and approved by a two thirds majority of those present and voting.</li>
                <li>The Secretary will maintain a record of the  attendees at the General Assembly by an attendance sheet. </li>
                <li>The General Assembly shall be quorate if duly  called as by (b) and (c) above.</li>
                <li>The decisions and votes taken shall be minuted  by the outgoing Secretary and signed by the incoming President. The minutes  shall be circulated to all members and reviewed and approved by the next  General Assembly.</li>
              </ol>

              <h3>7. HEADQUARTERS</h3>
              <p>The address of OPI  shall be determined by the Board.</p>

              <h3>8. ACCOUNTS</h3>
              <ol type="a">
                <li>The  funds of OPI including all membership fees, donations, contributions and  bequests, shall be paid into an account operated by the Board. The Treasurer  must authorise all expenses by email, circulated to the Board.</li>
                <li>The funds belonging to the group shall be  applied only to further the aims of the group. </li>
                <li>A current record of all income, funding and  expenditure will be kept.</li>
                <li>The auditor must make a recommendation to  approve or reject the accounts at the General Assembly.</li>
              </ol>

              <h3>9. DISSOLUTION</h3>
              <ol type="a">
                <li>OPI may be dissolved by a resolution passed by a  simple two-thirds majority of those present and voting at a Special General Assembly.</li>
                <li>If confirmed, the Governing Board shall  distribute any assets remaining after the payment of all bills to other  charitable group(s) or organisation(s) having aims similar to OPI or some other  charitable purpose(s) as OPI may decide at the General Assembly.</li>
              </ol>
              <p>&nbsp;</p>
              <p><em><strong>The Constitution is Signed and Dated by the President, the Secretary and the Treasurer of OPI.</strong></em></p>
            </div>
          </div>
        </div>
      </aside>

    </main>

  </div>
  <?php include 'footer.php';?>
</body>
