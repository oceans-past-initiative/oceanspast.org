<div class="agenda">
  <div class="project article">
    <div id="">
      <img src="assets/img/pub.jpg" alt="image of sea shells on beach">
      <p>This page features recent publications by scientists working within the field of marine environmental history / historical marine ecology. While this list does not pretend to cover everything of relevance, we do encourage submissions to the list below from anyone interested.&nbsp;Submissions can be made via email to <a href="mailto:bpoulsen@cgs.aau.dk">Bo Poulsen</a> or <a href="mailto:escolademar@gmail.com">Cristina Brito</a>.</p>

      <p>Please use the bibliographic format in this example:<br />
      Schwerdtner-M&aacute;nez K., Holm P., Blight L., Coll M., MacDiarmid A., Ojaveer H., Poulsen B., Tull M. 2014. The future of the oceans past: towards a global marine historical research initiative. PLoS ONE 9(7): e101466. doi:10.1371/journal.pone.0101466<br />
      &nbsp;&nbsp;<br />
      NB: Practioners in the field are also encouraged to consider publishing in&nbsp;<em><u><a href="http://www.hull.ac.uk/hmap/Library/Library.htm">PLoS ONE</a></u></em><a href="http://www.hull.ac.uk/hmap/Library/Library.htm">: the HMAP Collection</a>.</p>

      <h2>2017</h2>
      <ul>
        <li>Rieser A (2017). The Herring Enlightenment: Adam Smith and the Reform of British Fishing Subsidies, 1783-1799. International Journal of Maritime History, 29(3): 600-619. DOI: <a href="https://doi.org/10.1177/0843871417708182">https://doi.org/10.1177/0843871417708182</a></li>
        <li>Till Markus, Helmut Hillebrand, Anna-Katharina Hornidge, Gesche Krause, Achim Schlüter, ; Disciplinary diversity in marine sciences: the urgent case for an integration of research, ICES Journal of Marine Science, , fsx201, <a href="https://doi.org/10.1093/icesjms/fsx201">https://doi.org/10.1093/icesjms/fsx201</a></li>
        <li>Vieira N & Brito C (2017). Brazilian manatees (re)discovered: Early modern accounts reflecting the overexploitation of aquatic resources and the emergence of conservation concerns. International Journal of Maritime History, 29(3): 513-528. <a href="http://journals.sagepub.com/doi/10.1177/0843871417713683">DOI: https://doi.org/10.1177/0843871417713683</a></li>
      </ul>

      <h2>2016</h2>
      <ul>
        <li>Schwerdtner Máñez, Kathleen & Poulsen, Bo (Eds.) Perspectives on Oceans Past. A Handbook of Marine Environmental History.  Springer Science+Business Media, Dordrecht: 175-191. <a href="http://www.springer.com/us/book/9789401774956#">http://www.springer.com/us/book/9789401774956#</a> </li>
        <li>Brito C, Vieira N, Jordão V & Teixeira A (2016). Digging into our whaling past: Addressing the Portuguese influence in the early modern exploitation of whales in the Atlantic. In Environmental History in the Making. Volume II: Acting. Volume 7 of the Series Environmental History. Springer International Publishing. Switzerland: 33-47 pp. Doi: 10.1007/978-3-319-41139-2_3</li>
      </ul>

      <h2>2015</h2>
      <ul>
        <li>Brito C, Jordão V & Pierce G (2015). The environmental history and economy of ambergris: Portuguese sources contributing to study the trade of a global commodity. The Journal of Marine Biological Association of the United Kingdom, 96 (3): 585-596. DOI: 10.1017/S0025315415000910. </li>
        <li>Monsarrat S, Pennino MG, Smith TD, Reeves RR, Meynard CN, Kaplan DM &amp; Rodrigues ASL (2015) Historical summer distribution of the endangered North Atlantic right whales (Eubalaena glacialis): a hypothesis based on environmental preferences of a congeneric species. Biodiversity and Distributions. 1-13.&nbsp;<a href="http://onlinelibrary.wiley.com/doi/10.1111/ddi.12314/abstract">Biodiversity and Distributions, Early View </a></li>
      </ul>

      <h2>2014</h2>
      <ul>
        <li>Aguilar A &amp; Sandberg OR (2014) Norwegians in Iberia. The Compa&ntilde;ia Ballenera Espa&ntilde;ola (1914-1927). In JA Ringstad (Ed) Whaling and History IV. Kommander Chr. Christensens Hvalfanstmuseum, Sandefjord (Norway).</li>
        <li>Brito, C. &amp; Jord&atilde;o, V. (2014) A baleacion medieval e moderna em Portugal: Que nos din as fontes hist&oacute;ricas? Eubalaena, n&deg; 14: 28-40.</li>
        <li>Christensen, J., and Tull, M (eds.) (2014) Historical Perspectives on Fisheries Exploitation in the Indo-Pacific. Dordrecht: Springer Verlag (2014).</li>
        <li>Christensen, J., and Tull, M. (2014) Introduction: Historical Perspectives of Fisheries Exploitation in the Indo-Pacific. In Christensen, J and Tull, M., (eds.) (2014) Historical Perspectives on Fisheries Exploitation in the Indo-Pacific. Dordrecht: Springer Verlag pp.1-12.&nbsp;</li>
        <li>Engelhard GH, Righton DA, Pinnegar JK (2014) Climate change and fishing: a century of shifting distribution in North Sea cod. Global Change Biology 20: 2473-2483. doi: 10.1111/gcb.12513</li>
        <li>Engelhard GH (accepted) On the need to study fishing power change: challenges and perspectives. In: Schwerdtner Ma&ntilde;ez K, Poulsen B (eds) Perspectives in Oceans Past: A Marine Environmental History. Springer Publishers.</li>
        <li>Fortibuoni T. 2013. Come cambia il mare? Dagli archivi, la storia dell&rsquo;Adriatico. Chioggia, Rivista di Studi e Ricerche. n. 42, aprile 2013, pp. 97-111.</li>
        <li>Fortibuoni T., Fiorentino F. 2013. Conflitti ed emergenze nella pesca. La storia dei pescatori chioggiotti e gli spunti per un diverso sviluppo della pesca. In &ldquo;In mare altrui. Pesca e territorialit&agrave; in ambito interdisciplinare&rdquo; by Bulian G. and Raicevich S., Aracne editrice (Roma), 229-262 (ISBN 978-88-548-6600-3).</li>
        <li>Fortibuoni T., Gertwagen R., Giovanardi O., Raicevich S. 2014. The Progressive Deregulation of Fishery Management in the Venetian Lagoon After the Fall of the Repubblica Serenissima: Food for Thought on Sustainability. Global Bioethics 25(1): 42-55. doi:10.1080/11287462.2014.894707.</li>
        <li>Holm, P, Commercial Sea Fisheries in the Baltic Region, c1000-1600 in, James H. Barrett (ed), Cod and Herring: The Chronology, Causes and Consequences of Medieval Sea Fishing , Oxford, Oxbow, 2014 (forthcoming)</li>
        <li>Holm, P, Historical fishing communities in, editor(s)K. Schwerdtner M&aacute;&ntilde;ez &amp; B. Poulsen (eds), Perspectives in Oceans Past: A Marine Environmental History. Springer Publishers., Springer, 2014 (forthcoming)</li>
        <li>Holm, P, Learning from Asian and Indo-Pacific fisheries history in, editor(s)Joseph Christensen, Malcolm Tull , Historical Perspectives of Fisheries Exploitation in the Indo-Pacific, Dordrecht, Netherlands, Springer Science+Business Media , 2014, pp269 - 272</li>
        <li>Holm, P. (2014) Review of 'The Mortal Sea: Fishing the Atlantic in the Age of Sail' , by W. Jeffrey Bolster , H-Environment Roundtable Review, 4, (3): 1-4 [<a href="https://networks.h-net.org/system/files/contributed-files/env-roundtable-4-3_1.pdf">https://networks.h-net.org/system/files/contributed-files/env-roundtable-4-3_1.pdf</a>]</li>
        <li>Metcalf, S.J., van Putten, I., Frusher, S.D, Tull, M., Marshall, N. (2014) Adaptation options for marine industries and coastal communities using community structure and dynamics. Sustainability Science, Vol 9:247-261.</li>
        <li>Orton DC, Morris J, Locker A and Barrett JH (2014) Fish for the City: meta-analysis of archaeological cod remains as a tool for understanding the growth of London's northern trade. Antiquity 88, 516-30.</li>
        <li>Raicevich S., Fortibuoni T. 2013. Assessing neoextirpations in the Adriatic Sea: an historical ecology approach. &quot;Marine extinctions - Patterns and processes&quot; CIESM Workshop Monograph n&deg;45 (F. Briand ed.). CIESM Publisher, Monaco: pp. 97-111.</li>
        <li>Saporiti F, Bala LO, G&oacute;mez-Otero J, Crespo EA, Piana EL, Aguilar A, Cardona L (2014) Palaeoindian pinniped exploitation in South America was driven by oceanic productivity. Quaternary International, DOI: 10.1016/j.quaint.2014.05.015</li>
        <li>Schwerdtner M&aacute;&ntilde;ez K, Holm P, Blight L, Coll M, MacDiarmid A, Henn Ojaveer H, Poulsen B, Tull M. (2014) The Future of the Oceans Past: Towards a Global Marine Historical Research Initiative. PLoS ONE 9(7): e101466. doi:10.1371/ journal.pone.0101466.</li>
        <li>Teixeira A, Ven&acirc;ncio R &amp; Brito C (2014) Archaeological remains accounting for the presence and exploitation of the North Atlantic right whale Eubalaena glacialis on the Portuguese coast (Peniche, West Iberia), 16th to 17th century. PLOS ONE, 9(2): e85971. (doi:10.1371/journal.pone.0085971).</li>
        <li>Tull, M. (2014) &lsquo;History of Shark fishing in Indonesia&rsquo;, in: J. Christensen and M. Tull(eds.), Historical Perspectives on Fisheries Exploitation in the Indo-Pacific. Dordrecht: Springer Verlag, pp.63-81.</li>
        <li>Van Putten, Ingrid; Metcalf, Sarah; Frusher, Stewart; Marshall, Nadine and Tull, Malcolm (2014) Transformation of coastal communities: Where is the marine sector heading? [online]. Australasian Journal of Regional Studies, The, Vol. 20, No. 2: 286-324.</li>
      </ul>

      <h2>2013</h2>
      <ul>
        <li>Aguilar A (2013) Chim&aacute;n, la pesca ballenera moderna en la Pen&iacute;nsula Ib&eacute;rica. Publicacions de la Universitat de Barcelona, 375 pp. ISBN: 978-84-475-3763-1.</li>
        <li>Amorim I (2013) Towards a census of marine life: Guidelines for a marine environmental history of Portugal. In I Heidbrink &amp; M McCarthy (Eds) Fisheries Management in a Historical Perspective. Studia Atlantica, 14 (NAFHA Association Publications Series): 165-184. ISBN: 978-0-9545027-7-5.</li>
        <li>Fincham JI, Rijnsdorp AD, Engelhard GH (2013) Shifts in the timing of spawning in sole linked to warming sea temperatures. Journal of Sea Research 75: 69-76. doi: 10.1016/j.seares.2012.07.004</li>
        <li>Holm, P; Marta Coll; Alison MacDiarmid; Henn Ojaveer; Bo Poulsen (2013) HMAP Response to the Marine Forum, Environmental History, 18, (1): 121 - 126</li>
        <li>Holm, P. (2013) World War II and the 'Great Acceleration' of North Atlantic fisheries, Global Environment, 10: 66&nbsp;-&nbsp;91</li>
        <li>J&oslash;rgensen, F. A., Karlsdottir, U. B., M&aring;rald, E., Poulsen, B. &amp; S&auml;r&auml;nen, T, (2013) Entangled Environments: Historians and Nature in the Nordic Countries, in: Historisk Tidsskrift, 92 (1): 9-34.</li>
        <li>Kerby T.K., Cheung W.W.L., van Oosterhout C., Engelhard G.H. (2013) Entering uncharted waters: long-term dynamics of two data limited fish species, turbot and brill, in the North Sea. Journal of Sea Research 84: 87-95. doi:10.1016/j.seares.2013.07.005</li>
        <li>Kerby T.K., Cheung W.W.L., van Oosterhout C., Engelhard G.H. (2013) Wondering about wandering whiting: distribution of North Sea whiting between the 1920s and 2000s. Fisheries Research 145: 54-65. doi: 10.1016/j.fishres.2013.02.012</li>
        <li>Poulsen, B. (2013), &rdquo;Nye Blade til Danmarks &AElig;reskrans&rdquo;. - Havforsker Johannes Schmidt som fundraiser og mediestrateg i 1920erne, Carlsbergfondet &aring;rsskrift: 54-59.</li>
        <li>Prieto R, Pham C, Brito C &amp; Morato T (2013) Biomass removal from shore-based whaling in the Azores. Fisheries Research, 143: 98-101. (<a href="http://dx.doi.org/10.1016/j.fishres.2013.02.001">http://dx.doi.org/10.1016/j.fishres.2013.02.001</a>)</li>
        <li>Saporiti F, Bala LO, Crespo EA, Gómez-Otero J, Zangrando AFJ, Aguilar A, Cardona L (2013) Changing patterns of marine resource exploitation by hunter-gatherers throughout the late Holocene of Argentina are uncorrelated to sea surface temperature. Quaternary Internationa,, 299: 108-115.</li>
        <li>Van Duzer C. (2013) Sea monsters on maps - The transition from the Middle Ages to the Renaissance. BIMCC Newsletter 46: 18-19.</li>
        <li>Van Duzer C. (2013) Sea monsters on medieval and renaissance maps. London: British Library.</li>
      </ul>

      <h2>2012</h2>
      <ul>
        <li>Amorim I &amp; Lopez-Losa E (2012) The fisheries of the Iberian Peninsula in modern times. In D. Starkey &amp; H. Heidbrink (Eds) A history of the North Atantic fisheries: Volume 2: from the 1850s to the early twenty-first century. Bremen: NAFHA/German Maritime Museum: 254-276.</li>
        <li>Boehme L., Thompson D., Fedak M., Bowen D., Hammill M.O., Stebson G.B. (2012) How many seals were there? The global shelf loss during the last glacial maximum and its effect on the size and distribution of grey seal populations. PLoS ONE 7(12): e53000. doi: 10.1371/journal.pone.0053000.&nbsp;</li>
        <li>Brito C. (2012) Portuguese Sealing and Whaling Activities as Contributions to Understand Early Northeast Atlantic Environmental History of Marine Mammals. In New Approaches to the Study of Marine Mammals, Aldemaro Romero and Edward O. Keith (Ed.), ISBN: 978-953-51-0844-3, InTech: 207-222. (DOI: 10.5772/54213)</li>
        <li>Engelhard, G. &amp; Poulsen, B. (eds.), (2012) Report of the Study Group on the History of Fish and Fisheries (SGHIST), 24-27 October 2011, Cefas, Lowestoft, UK. Copenhagen: &nbsp;International Council for the Exploration of the Sea, 2012. 36 p. (ICES CM 2011/SSGSUE:11. 36 pp.).</li>
        <li>Kerby TK, Cheung WWL, Engelhard GH (2012) The United Kingdom&rsquo;s role in North Sea demersal fisheries: a hundred year perspective. Reviews in Fish Biology and Fisheries 22: 621-634. doi: 10.1007/s11160-012-9261-y</li>
        <li>Poulsen, B. (2012) &lsquo;Fisheries&rsquo;, In: L. Kotz&eacute; &amp; S. Morse (eds.) The Berkshire Encyclopedia of Sustainability vol. 9. Afro-Eurasia: Assessing Sustainability, Great Barrington, MA: Berkshire Publishing Group, 136-140.</li>
        <li>Poulsen, B. (2012) Marin milj&oslash;historie - samfundsrelevant tv&AElig;rvidenskab, in: Enevoldsen, T. &amp; Jels&oslash;e, E. (eds.) Tv&AElig;rvidenskab i teori og praksis, K&oslash;benhavn: Hans Reitzels Forlag, 180-200.</li>
        <li>Poulsen, B. (2012) MARINE ENVIRONMENTAL HISTORY, in World Environmental History, [Eds. Mauro Agnoletti, Elizabeth Johann and Simone Neri Serneri], in Encyclopedia of Life Support Systems (EOLSS), Developed under the Auspices of the UNESCO, Eolss Publishers, Oxford, UK, http://www.eolss.net </li>
        <li>Poulsen, B. (2012) Orange brille : les nombreuses tentatives pour imiter le mod&egrave;le des p&ecirc;cheries n&eacute;erlandaises de harengs en mer du Nord et dans la Baltique (XVI&deg;-XIX&deg; siècles). In: Revue d'Histoire Maritime, 15, 131-160.</li>
        <li>Van Duzer C. (2012) I mostri marini nel manoscritto di Madrid della Geografia di Tolomeo (Biblioteca Nacional, MS Res. 255). Geostorie 20. 1-3: 113-132.</li>
      </ul>

      <h2>2011</h2>
      <ul>
        <li>Amorim I (2011) Sea salt and land salt. The language of salt and technology transfer (Portugal since the second half of the 18th century). In M Alexianu, O Weller, RG Curc&atilde; (Eds) Archaeology and anthropology of salt: A diachronic approach. Oxford: British Archaeological Reports - International Series 2198: 187-195. ISBN: 978-1-4073-0754-1.</li>
        <li>Barrett J., Orton D.C., Johnstone C., Harland J., Van Neer W., Ervynck A et al. (2011) Interpreting the expansion of sea fishing in medieval Europe using stable isotope analysis of archaeological cod bones. Journal of Archaeological Science 38, 1516-24.</li>
        <li>Blight, L. (2011) Egg Production in a Coastal Seabird, the Glaucous-Winged Gull (Larus glaucescens), Declines during the Last Century, PLoS ONE: The HMAP Collection, DOI: 10.1371/journal.pone.0022027</li>
        <li>Brito, C. (2011) Medieval and early modern whaling in Portugal. Anthrozoos, 24(3): 287-300.</li>
        <li>Brito, C. &amp; Sousa, A. (2011) The environmental history of cetaceans in Portugal: Ten centuries of whale and dolphin records. PLoS ONE: The HMAP Collection, 6(9): e23951.</li>
        <li>Engelhard G.H., Pinnegar J.K., Kell L.T., Rijnsdorp A.D. (2011) Nine decades of North Sea sole and plaice distribution. ICES Journal of Marine Science 68: 1090-1104. doi: 10.1093/icesjms/fsr031</li>
        <li>Engelhard G.H., Ellis J.R., Payne M.R., ter Hofstede R., Pinnegar J.K. (2011) Ecotypes as a concept for exploring responses to climate change in fish assemblages. ICES Journal of Marine Science 68: 580-591. doi: 10.1093/icesjms/fsq183.</li>
        <li>Fortibuoni T., Libralato S., Solidoro C., Giovanardi O., Raicevich S. 2011. Changes in marine communities of the Northern Adriatic Sea (Mediterranean) over the past six decades (1945-2008): insights from fisheries landings. ICES CM 2011/D:12.</li>
        <li>Giovanardi O., Fortibuoni T., Raicevich S. 2011. Fishing techniques and traditions in Italian administrative regions. In &ldquo;The state of Italian marine fisheries and aquaculture&rdquo;. Eds. Stefano Cataudella and Massimo Spagnolo (Ministero delle Politiche Agricole, Alimentari e Forestali - MiPAAF, Rome), pp. 206-208.</li>
        <li>Orton D.C., Makowiecki D., de Roo T., Johnstone C., Harland J., Jonsson L et al. (2011) Stable Isotope Evidence for Late Medieval (14th-15th C) Origins of the Eastern Baltic Cod (Gadus morhua) Fishery. PLoS ONE 6, e27568.</li>
        <li>Sousa, A. &amp; Brito, C. (2011) Historical strandings of cetaceans on the Portuguese coast: Anecdotes, people and naturalists. Marine Biodiversity Records, 4: e102.</li>
        <li>Van Duzer C. (2011) The sea monsters in the Madrid manuscript of Ptolomy's Geography (Biblioteca Nacional, MS Res. 255). Word &amp; Image 27.1: 115-123.</li>
        <li>Van Lottum, J. &amp; Poulsen, B. (2011) Estimating levels of numeracy and literacy in the maritime sector of the North Atlantic in the late eighteenth century, Scandinavian Economic History Review, 59, (1), 67-82</li>
      </ul>

      <h2>2010</h2>
      <ul><li>Amorim I (2010) L'exploitation de la mer et de l'estran: un bilan comparatif vu par l'historiographie portuguaise. Revue d'Histoire Maritime: La recherche internationale en histoire maritime: essai d'&eacute;valuation, n&deg; 10-11: 285-310. Paris-PUPS. ISBN: 978-2-84050-698-0.</li>
        <li>Beare D., H&ouml;lker F., Engelhard G.H., McKenzie E., Reid D. (2010) An unintended experiment in fisheries science: a marine area protected by war results in Mexican waves in fish numbers-at-age. Naturwissenschaften 97: 797-808. doi: 10.1007/s00114-010-0696-5</li>
        <li>Brito, C. &amp; Vieira, N. (2010) Using historical accounts to assess the occurrence and distribution of small cetaceans in a poorly known area. Journal of the Marine Biological Association of the UNited Kingdom, 90(8): 1583-1588.</li>
        <li>Engelhard G.H. (2008) One hundred and twenty years of change in fishing power of English North Sea trawlers. In: Payne A, Cotter J, Potter T (eds) Advances in Fisheries Science 50 Years on from Beverton and Holt. Blackwell Publishing, pp 1-25.</li>
        <li>Fortibuoni T., Libralato S., Raicevich S., Giovanardi O., Solidoro C. 2010. Coding Early Naturalists' Accounts into Long-Term Fish Community Changes in the Adriatic Sea (1800-2000). PLoS ONE 5(11): e15502. doi:10.1371/journal.pone.0015502.</li>
        <li>Fortibuoni T. 2010. Fishery in the Northern Adriatic Sea from the Serenissima fall to present: an historical and ecological perspective. In &ldquo;When Humanities Meet Ecology: Historic changes in Mediterranean and Black Sea marine biodiversity and ecosystems since the Roman period until nowadays. Languages, methodologies and perspectives. Proceedings of the HMAP International Summer School. 31st August - 4th September 2009, Trieste (Italy). ISPRA Serie Atti 2010, Rome&rdquo;pp.147&nbsp;156.</li>
        <li>Fortibuoni T., Raicevich S. 2010. Una crociera di ricerca in Montenegro e Albania, ovvero note sulla ricerca per la gestione della pesca in Adriatico, dall&rsquo;inizio del &lsquo;900 ad oggi. Chioggia, Rivista di Studi e Ricerche, 36: 121-130.</li>
        <li>Gertwagen R., Fortibuoni T., Giovanardi O., Libralato S., Solidoro C., Raicevich S. (Eds.) 2010. When Humanities Meet Ecology: Historic changes in Mediterranean and Black Sea marine biodiversity and ecosystems since the Roman period until nowadays. Languages, methodologies and perspectives. Proceedings of the HMAP International Summer School. 31st August - 4th September 2009, Trieste (Italy). ISPRA Serie Atti 2010, Rome, pp. 360.</li>
        <li>Holm P., Anne Husum Marboe, Bo Poulsen, Brian R. MacKenzie (2010) Marine Animal Populations: A New Look Back Iin Time in, editor(s)Alasdair D. McIntyre , Life in the World's Oceans: Diversity, Distribution, and Abundance , Oxford, Blackwell: p3 - 23</li>
        <li>Holm P. (2010) Oceans and seas, William H. McNeill et al., Berkshire Encyclopedia of World History, 2nd edition, Berkshire Publishing Group: 1860 - 1866</li>
        <li>Pinnegar J.K., Engelhard G.H. (2008) The &lsquo;shifting baseline&rsquo; phenomenon: a global perspective. Reviews in Fish Biology and Fisheries 18: 1-16. doi: 10.1007/s11160-007-9058-6</li>
        <li>Poulsen, B. (2010) The variability of fisheries and fish populations prior to industrialized fishing: An appraisal of the historical evidence. Journal of Marine Systems, 79, &nbsp;3-4, 327-332 <a href="http://dx.doi.org/10.1016/j.jmarsys.2008.12.011">http://dx.doi.org/10.1016/j.jmarsys.2008.12.011</a> </li>
        <li>Raicevich S., Fortibuoni T., Franceschini G., Celic I., Giovanardi O. 2010. The use of Local Ecological Knowledge to reconstruct the history of marine animal populations. Potentials and limitations. In &ldquo;When Humanities Meet Ecology: Historic changes in Mediterranean and Black Sea marine biodiversity and ecosystems since the Roman period until nowadays. Languages, methodologies and perspectives. Proceedings of the HMAP International Summer School. 31st August - 4th September 2009Trieste (Italy). ISPRA Serie Atti 2010, Rome&rdquo;, pp. 81-94.</li>
        <li>Vieira, N. &amp; Brito, C. (2010) Past and recent sperm whale sightings in the Azores based on catches and whale watching information. Journal of the Marine Biological Association of the United Kingdom, 89(5): 1067-1070.</li>
      </ul>
    </div>
  </div>
</div>
