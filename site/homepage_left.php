  <div id="prolog"></div>
  <div class="about">
    <h1 class="intro">Welcome to the OPI</h1>
    <p class="intro square-text">The Oceans Past Initiative (OPI) is a global research network for marine historical research. Our goal is to enhance knowledge and understanding of how the diversity, distribution and abundance of marine life in the world’s oceans has changed over the long term to better indicate future changes and possibilities. </p>
  </div>
