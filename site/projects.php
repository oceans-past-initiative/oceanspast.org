<html>
<title>all the OPI projects</title>
<meta property="og:title" content="OPI Projects"/>
<?php
if (file_exists('local.txt')) {
    //don't load admin headers
} else {
    // we are in production server
    include "login/misc/pagehead.php";
} ?>
  <?php include 'header.php';?>
<body>
  <?php include 'nav.php';?>

<div id="container">
    <main>
      <aside class="left">
        <?php include 'projects_left.php';?>
      </aside>
      <aside class="right">
        <?php include 'projects_body.php';?>
      </aside>

    </main>

  </div>
  <?php include 'footer.php';?>
</body>
