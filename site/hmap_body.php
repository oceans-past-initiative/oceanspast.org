<div class="agenda">
  <div class="project article">
    <div id="">

      <img src="assets/img/hmap.jpg" width="100%" alt="hmap header image" />
      <div class="info-box-maintext">
        <p><a href="http://www.hull.ac.uk/hmap/Library/Library.htm">The Data Store contains two types of downloadable dataset:</a></p>
        <ol>
          <li>Processed HMAP Datasets</li>
          <li>Data Files (in process)</li>
        </ol>

        <p>The Data Search facility enables visitors to search across the Processed HMAP Datasets and to construct their own customised datasets.</p>
        <p>The History of Marine Animal Populations Project (2000-2010) resulted in an abundance of new datasets on historical marine resource use, and much of these data can be downloaded freely from the website of the HMAP Data Pages:<br />
        <a href="http://www.hull.ac.uk/hmap/Library/Library.htm">The HMAP Data Pages</a> are a research resource comprising of information derived largely from historical records relating to fishing catches and effort in selected spatial and temporal contexts.</p>

        <h3>PLOS ONE: HMAP Coll</h3>
        <p>The Oceans Past Initiative is the main stakeholder in the <a href="http://www.ploscollections.org/article/browse/issue/info%3Adoi%2F10.1371%2Fissue.pcol.v02.i10"><em>PLoS ONE</em>: The HMAP Collection</a>. Arising from the History of Marine Animal Populations (HMAP) project, this collection forms an entry point for historical marine ecology / marine environmental history.</p>
        <p>The overall goal is to enhance knowledge and understanding of how the diversity, distribution and abundance of marine life in the world's oceans has changed over the long term. The HMAP Collection draws together representative examples of the results of this initiative. Articles are presented in order of publication date and new articles will be added to the Collection as they are published.</p>
        <p>All datasets have Supporting Documentation which includes information on the objectives of the data collection effort, the provenance of the historical source material from which the data are derived, the structure of the dataset, and the outputs that have been generated. These documents have been drafted by the data providers and edited by Michaela Barnard, John Nicholls and David Starkey.</p>

        <h3>How to Download Datasets</h3>
        <p>If you require assistance with downloading, or information about how to view or open the files, select one of the options here:</p>

        <h4><a href="assets/pdfs/hmap/HowtoDownload.doc">Word</a><b> | </b><a href="assets/pdfs/hmap/HowtoDownload.pdf">PDF</a></h4>

        <h2>Processed HMAP Datasets</h2>
        <p>All Datasets: Search, download and reference HMAP Datasets with HYDRA</p>

        <table width="60%" border="none">
            <tbody>
            <tr bgcolor="#D4FFFF">
              <td width="83"><a href="https://hydra.hull.ac.uk/resources/hull:1934" title="Dataset 1" target="_blank">Dataset 01</a></td>
              <td width="576"><a href="https://hydra.hull.ac.uk/resources/hull:1934" title="Dataset 1" target="_blank"> SE Australian Trawl Fishery I: Fish (various) landings and fishing effort, South East Australia, 1918-1923</a></td>
            </tr>
            <tr bgcolor="#CCFF99">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:1935" title="Dataset 2" target="_blank">Dataset 02</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:1935" title="Dataset 2" target="_blank">SE Australian Trawl Fishery II: Fish (various) landings and fishing effort, South East Australia, 1937-1943</a></td>
            </tr>
            <tr bgcolor="#C0DCC0">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:1936" target="_blank">Dataset 03</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:1936" target="_blank"> SE Australian Trawl Fishery III: Fish (various) landings and fishing effort, South East Australia, 1951-1957</a></td>
            </tr>
            <tr bgcolor="#D4FFFF">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2155" target="_blank">Dataset 04</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2155" target="_blank"> World Whaling</a></td>
            </tr>
            <tr bgcolor="#CCFF99">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2156" target="_blank">Dataset 05</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2156" target="_blank"> Newfoundland, 1698-1833</a></td>
            </tr>
            <tr bgcolor="#C0DCC0">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2157" target="_blank">Dataset 06</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2157" target="_blank"> Newfoundland, 1675-1698</a></td>
            </tr>
            <tr bgcolor="#D4FFFF">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2159" target="_blank">Dataset 07</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2159" target="_blank">Danish Baltic Catch Data, 1611-1920</a></td>
            </tr>
            <tr bgcolor="#CCFF99">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2160" target="_blank">Dataset 08</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2160" target="_blank">Swedish Baltic Catch Data, 1752-1990</a></td>
            </tr>
            <tr bgcolor="#C0DCC0">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2161" target="_blank">Dataset 09</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2161" target="_blank">North Russian Salmon Catch Data, 1615-1937</a></td>
            </tr>
            <tr bgcolor="#D4FFFF">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2162" target="_blank">Dataset 10</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2162" target="_blank">Catalonian Coast</a></td>
            </tr>
            <tr bgcolor="#CCFF99">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2163" target="_blank">Dataset 11</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2163" target="_blank">Limfjord Fisheries</a></td>
            </tr>
            <tr bgcolor="#C0DCC0">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2164" target="_blank">Dataset 12</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2164" target="_blank">North-west Scotland Fisheries</a></td>
            </tr>
            <tr bgcolor="#D4FFFF">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2165" target="_blank">Dataset 13</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2165" target="_blank">Galapagos Marine Reserve, Ecuador I</a></td>
            </tr>
            <tr bgcolor="#CCFF99">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2166" target="_blank">Dataset 14</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2166" target="_blank">Galapagos Marine Reserve, Ecuador II</a></td>
            </tr>
            <tr bgcolor="#C0DCC0">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2167" target="_blank">Dataset 15</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2167" target="_blank">Galapagos Marine Reserve, Ecuador III</a></td>
            </tr>
            <tr bgcolor="#D4FFFF">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2168" target="_blank">Dataset 16</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2168" target="_blank">Peru, South-east Pacific </a></td>
            </tr>
            <tr bgcolor="#CCFF99">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2169" target="_blank">Dataset 17</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2169" target="_blank"> North Sea Demersal Fish </a></td>
            </tr>
            <tr bgcolor="#C0DCC0">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2170" target="_blank">Dataset 18</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2170" target="_blank"> Colombian Caribbean Sea </a></td>
            </tr>
            <tr bgcolor="#D4FFFF">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2171" target="_blank">Dataset 19</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2171" target="_blank"> Estonian Fisheries: Pärnu City Customs Records </a></td>
            </tr>
            <tr bgcolor="#CCFF99">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2172" target="_blank">Dataset 20</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2173" target="_blank">Limfjord Catch Data, 1890-1925 </a></td>
            </tr>
            <tr bgcolor="#C0DCC0">
              <td><a href="https://edocs.hull.ac.uk/muradora/objectView.action?parentId=hull%3A1933&amp;type=1&amp;start=20&amp;pid=hull%3A2173" target="_blank">Dataset 21</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2173" target="_blank"> Gulf of Thailand cephalopod Fisheries </a></td>
            </tr>
            <tr bgcolor="#D4FFFF">
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2174" target="_blank">Dataset 22</a></td>
              <td><a href="https://hydra.hull.ac.uk/resources/hull:2174" target="_blank">Catch and Fishing Effort in the Limfjord Fisheries </a></td>
            </tr>
          </tbody>
          </table>

          <h2>Data Files (In Process)</h2>
          <table width="99%" border="0">
            <tbody><tr>
              <td colspan="6" bgcolor="#D4BFFF" class="style5"><span class="style10"><a name="s1" id="s1"></a>Data File (in process) 1: Gulf of Maine Cod Catches,&nbsp;1861-1864</span></td>
            </tr>
            <tr>
              <td height="32" bgcolor="#D4BFFF" class="bodyText2"><div align="right"><strong>Data File </strong>&nbsp;&nbsp;&nbsp;</div></td>
              <td align="center" bgcolor="#D4BFFF" class="bodyText2">Excel (<a href="assets/InProcess1/Alexander_GulfofMaine.xls" target="_blank">0.76Mb</a>)</td>
              <td align="center" bgcolor="#D4BFFF" class="bodyText2">&nbsp;</td>
              <td width="20%" colspan="3" align="center" bgcolor="#D4BFFF" class="bodyText2">&nbsp;</td>
            </tr>
            <tr>
              <td width="42%" height="32" bgcolor="#D4BFFF" class="bodyText2"><div align="right"><strong>Supporting Documentation</strong>&nbsp;&nbsp;&nbsp;</div></td>
              <td width="19%" align="center" bgcolor="#D4BFFF" class="bodyText2">Word (<a href="assets/InProcess1/Suppdoc_datafile_inprocess_1.doc">0.11Mb</a>)</td>
              <td width="19%" align="center" bgcolor="#D4BFFF" class="bodyText2">PDF (<a href="assets/InProcess1/Suppdoc_datafile_inprocess_1.pdf" target="_blank">0.28Mb</a>)</td>
              <td colspan="3" align="center" bgcolor="#D4BFFF" class="bodyText2">Text (<a href="assets/InProcess1/Suppdoc_datafile_inprocess_1.txt" target="_blank">0.03Mb</a>)</td>
            </tr>
            <tr>
              <td colspan="6" bgcolor="#D4BFFF" class="bodyText2"><strong>Reference/Citation</strong>:<br>
                (a) <em>The dataset: please cite as follows:</em><br>
                K. Alexander ‘Gulf of Maine, Cod Catches, 1861-1864’ in M.G Barnard  &amp; J.H Nicholls (comp.) <em>HMAP Data Pages</em> (www.hull.ac.uk/hmap)<br>
                (b) <em>Supporting documentation: please cite as follows:</em><br>
                D.J. Starkey, ‘HMAP Data  File (in process) 1: Gulf   of Maine, Cod Catches,  1861-1864’, Supporting Documentation’, in M.G Barnard  &amp; J.H Nicholls (comp.) <em>HMAP Data Pages</em> (www.hull.ac.uk/hmap)</td>
            </tr>
          </tbody></table>
          <table width="99%" border="0">
            <tbody><tr>
              <td colspan="6" class="style5">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="6" bgcolor="#D4FFFF" class="style5"><span class="style10"><a name="s2" id="s2"></a>Data File (in process) 2: Gulf of Maine, Statistical Bulletins, 1898-1935</span></td>
            </tr>
            <tr>
              <td height="32" bgcolor="#D4FFFF" class="bodyText2"><div align="right"><strong>Data File</strong> &nbsp;&nbsp;&nbsp;</div></td>
              <td align="center" bgcolor="#D4FFFF" class="bodyText2">Excel (<a href="assets/InProcess2/HMAPortalData.zip" target="_blank">0.88Mb</a>)</td>
              <td align="center" bgcolor="#D4FFFF" class="bodyText2">&nbsp;</td>
              <td width="20%" colspan="3" align="center" bgcolor="#D4FFFF" class="bodyText2">&nbsp;</td>
            </tr>
            <tr>
              <td width="42%" height="32" bgcolor="#D4FFFF" class="bodyText2"><div align="right"><strong>Supporting Documentation</strong>&nbsp;&nbsp;&nbsp;</div></td>
              <td width="19%" align="center" bgcolor="#D4FFFF" class="bodyText2">Word (<a href="assets/InProcess2/Suppdoc_datafile_inprocess_2.doc" target="_blank">0.94Mb</a>)</td>
              <td width="19%" align="center" bgcolor="#D4FFFF" class="bodyText2">PDF (<a href="assets/InProcess2/Suppdoc_datafile_inprocess_2.pdf" target="_blank">1.11Mb</a>)</td>
              <td colspan="3" align="center" bgcolor="#D4FFFF" class="bodyText2">Text (<a href="assets/InProcess2/Suppdoc_datafile_inprocess_2.txt" target="_blank">0.02Mb</a>)</td>
            </tr>
            <tr>
              <td colspan="6" bgcolor="#D4FFFF" class="bodyText2"><strong>Reference/Citation</strong>:<br>
                (a) <em>The dataset: please cite as follows:</em><br>
                S. Claesson ‘Gulf of Maine,  Statistical Bulletins, 1898-1935’ in M.G Barnard  &amp; J.H Nicholls (comp.) <em>HMAP Data Pages</em> (www.hull.ac.uk/hmap)<br>
                (b) <em>Supporting documentation: please cite as follows:</em><br>
                D.J. Starkey, ‘HMAP Data File (in process) 2: Gulf of Maine, Statistical Bulletins, 1898-1935’, Supporting  Documentation’, in M.G Barnard  &amp; J.H Nicholls (comp.) <em>HMAP Data Pages</em> (www.hull.ac.uk/hmap)</td>
            </tr>
            <tr>
              <td colspan="6" class="bodyText2">&nbsp;</td>
            </tr>
          </tbody></table>
          <table width="99%" border="0" cellspacing="1" cellpadding="0">
            <tbody><tr>
              <td colspan="6" bgcolor="#C0DCC0" class="style5"><span class="style10"><a name="s3" id="s3"></a>Data File (in process) 3: North Sea, Swedish &amp; Danish Catch Data, 1840-1914</span></td>
            </tr>
            <tr>
              <td width="41%" height="32" bgcolor="#C0DCC0"><div align="right"><strong>Data Files </strong>&nbsp;&nbsp;&nbsp;</div></td>
              <td colspan="3" bgcolor="#C0DCC0">Access (<a href="assets/InProcess3/Danish_Skagerrak_fisheries_1888_1914.zip" target="_blank">0.53Mb</a>)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Danish Skagerrak fisheries, 1888-1914&nbsp;</td>
            </tr>
            <tr>
              <td height="32" bgcolor="#C0DCC0">&nbsp;</td>
              <td colspan="3" bgcolor="#C0DCC0">Access (<a href="assets/InProcess3/North_Sea_fisheries_1903_22_ICES.zip" target="_blank">0.08Mb</a>)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;North Sea fisheries, 1903-22, ICES</td>
            </tr>
            <tr>
              <td height="32" bgcolor="#C0DCC0">&nbsp;</td>
              <td colspan="3" bgcolor="#C0DCC0">Access (<a href="assets/InProcess3/Swedish_demersal_winter_fisheries_1886_1912.zip" target="_blank">0.06Mb</a>)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Swedish demersal, winter fisheries, 1886-1912&nbsp;</td>
            </tr>
            <tr>
              <td height="32" bgcolor="#C0DCC0">&nbsp;</td>
              <td colspan="3" bgcolor="#C0DCC0">Access (<a href="assets/InProcess3/Swedish_herring_and_sprat_fisheries_1885_1912.zip" target="_blank">0.12Mb</a>)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Swedish herring and sprat fisheries, 1885-1912&nbsp;</td>
            </tr>
            <tr>
              <td height="32" bgcolor="#C0DCC0">&nbsp;</td>
              <td colspan="3" bgcolor="#C0DCC0">Access (<a href="assets/InProcess3/Swedish_lobster_and_oyster_fisheries_1886_1912.zip" target="_blank">0.08Mb</a>)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Swedish lobster &amp; oyster fisheries, 1886-1912&nbsp;</td>
            </tr>
            <tr>
              <td height="32" bgcolor="#C0DCC0">&nbsp;</td>
              <td colspan="3" bgcolor="#C0DCC0">Access (<a href="assets/InProcess3/Swedish_longline_fisheries_1859_1886.zip" target="_blank">0.48Mb</a>)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Swedish longline fisheries, 1859-1886&nbsp;</td>
            </tr>
            <tr>
              <td height="32" bgcolor="#C0DCC0">&nbsp;</td>
              <td colspan="3" bgcolor="#C0DCC0">Access (<a href="assets/InProcess3/Swedish_longline_fisheries_1914.zip" target="_blank">0.04Mb</a>)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Swedish longline fisheries, 1914&nbsp;</td>
            </tr>
            <tr>
              <td height="32" bgcolor="#C0DCC0">&nbsp;</td>
              <td colspan="3" bgcolor="#C0DCC0">Access (<a href="assets/InProcess3/Swedish_mackerel_fisheries_1886_1912.zip" target="_blank">0.13Mb</a>)&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Swedish mackerel fisheries, 1886-1912&nbsp;</td>
            </tr>
            <tr>
              <td height="32" bgcolor="#C0DCC0"><div align="right"><strong>Supporting Documentation</strong>&nbsp;&nbsp;&nbsp;</div></td>
              <td width="19%" height="32" bgcolor="#C0DCC0"><div align="center"><span class="bodyText2">Word (<a href="assets/InProcess3/Suppdoc_datafile_inprocess_3.doc" target="_blank">0.94Mb</a>)</span></div></td>
              <td width="19%" height="32" bgcolor="#C0DCC0"><div align="center"><span class="bodyText2">PDF (<a href="assets/InProcess3/Suppdoc_datafile_inprocess_3.pdf" target="_blank">1.11Mb</a>)</span></div></td>
              <td width="21%" height="32" bgcolor="#C0DCC0"><div align="center"><span class="bodyText2">Text (<a href="assets/InProcess3/Suppdoc_datafile_inprocess_3.txt" target="_blank">0.02Mb</a>)</span></div></td>
            </tr>
            <tr>
              <td height="32" colspan="4" bgcolor="#C0DCC0"><span class="bodyText2"><strong>Reference/Citation</strong>:<br>
                (a) <em>The dataset: please cite as follows:</em><br>
                R.T.  Poulsen, ed., ‘North Sea, Swedish &amp; Danish Catch Data, 1840-1914’ in M.G Barnard  &amp; J.H Nicholls (comp.) <em>HMAP Data Pages</em>                  	                (www.hull.ac.uk/hmap)<br>
                (b) <em>Supporting documentation: please cite as follows:</em><br>
                R.T. Poulsen, ‘HMAP Data File: North Sea, Swedish &amp; Danish  Catch Data, 1840-1914, Supporting Documentation’, in M.G Barnard  &amp; J.H Nicholls (comp.) <em>                HMAP Data Pages</em> (www.hull.ac.uk/hmap)</span></td>
            </tr>
            <tr>
              <td colspan="4">&nbsp;</td>
            </tr>
          </tbody></table>
          <table width="60%" border="0">
            <tbody><tr>
              <td colspan="6" bgcolor="#D4BFFF" class="style5"><span class="style10"><a name="s4" id="s4"></a>Data File (in process) 4: White Sea Herring Data,
                1787-1954 </span></td>
            </tr>
            <tr>
              <td height="32" bgcolor="#D4BFFF" class="bodyText2"><div align="right"><strong>Data File </strong>&nbsp;&nbsp;&nbsp;</div></td>
              <td align="center" bgcolor="#D4BFFF" class="bodyText2">Excel (<a href="assets/InProcess4/Lajus_Russian_herring.xls" target="_blank">0.88Mb</a>)</td>
              <td align="center" bgcolor="#D4BFFF" class="bodyText2">&nbsp;</td>
              <td width="20%" colspan="3" align="center" bgcolor="#D4BFFF" class="bodyText2">&nbsp;</td>
            </tr>
            <tr>
              <td width="42%" height="32" bgcolor="#D4BFFF" class="bodyText2"><div align="right"><strong>Supporting Documentation</strong>&nbsp;&nbsp;&nbsp;</div></td>
              <td width="19%" align="center" bgcolor="#D4BFFF" class="bodyText2">Word (<a href="assets/InProcess4/Suppdoc_datafile_inprocess_4.doc" target="_blank">0.24Mb</a>)</td>
              <td width="19%" align="center" bgcolor="#D4BFFF" class="bodyText2">PDF (<a href="assets/InProcess4/Suppdoc_datafile_inprocess_4.pdf" target="_blank">0.41Mb</a>)</td>
              <td colspan="3" align="center" bgcolor="#D4BFFF" class="bodyText2">Text (<a href="assets/InProcess4/Suppdoc_datafile_inprocess_4.txt" target="_blank">0.02Mb</a>)</td>
            </tr>
            <tr>
              <td colspan="6" bgcolor="#D4BFFF" class="bodyText2"><strong>Reference/Citation</strong>:<br>
                (a) <em>The dataset: please cite as follows:</em><br>
                J. Lajus et al, eds., ‘White Sea Herring Data,  1787-1954’, in M.G Barnard  &amp; J.H Nicholls (comp.) <em>HMAP Data Pages</em> (www.hull.ac.uk/hmap)<br>
                (b) <em>Supporting documentation: please cite as follows:</em><br>
                J. Lajus et al, eds., ‘HMAP Data File: White Sea  Herring Data, 1787-1954, Supporting Documentation’, in M.G Barnard  &amp; J.H Nicholls (comp.) <em>HMAP Data                Pages</em> (www.hull.ac.uk/hmap)</td>
            </tr>
            <tr>
              <td colspan="6" class="style5">&nbsp;</td>
            </tr>
            <tr>
              <td colspan="6" bgcolor="#D4FFFF" class="style5"><span class="style10"><a name="s5" id="s5"></a>Data File (in process) 5: Database of American Offshore Whaling Voyages,                1667-1937</span></td>
            </tr>
            <tr>
              <td height="32" bgcolor="#D4FFFF" class="bodyText2"><div align="right"><strong>Data File</strong> &nbsp;&nbsp;&nbsp;</div></td>
              <td align="center" bgcolor="#D4FFFF" class="bodyText2">Excel (<a href="assets/InProcess5/AppendixB_American_Offshore_Whaling.xls" target="_blank">                3.20Mb</a>)</td>
              <td align="center" bgcolor="#D4FFFF" class="bodyText2">&nbsp;</td>
              <td colspan="3" align="center" bgcolor="#D4FFFF" class="bodyText2">&nbsp;</td>
            </tr>
            <tr>
              <td height="32" bgcolor="#D4FFFF" class="bodyText2"><div align="right"><strong>Supporting Documentation</strong>&nbsp;&nbsp;&nbsp;</div></td>
              <td align="center" bgcolor="#D4FFFF" class="bodyText2">Word (<a href="assets/InProcess5/American_Offshore_Whaling_Voyages.doc" target="_blank">                0.73Mb</a>)</td>
              <td align="center" bgcolor="#D4FFFF" class="bodyText2">PDF (<a href="assets/InProcess5/American_Offshore_Whaling_Voyages.pdf" target="_blank">                0.52Mb</a>)</td>
              <td colspan="3" align="center" bgcolor="#D4FFFF" class="bodyText2">Text (<a href="assets/InProcess5/American_Offshore_Whaling_Voyages.txt" target="_blank">       0.02Mb</a>)</td>
            </tr>
            <tr>
              <td colspan="6" bgcolor="#D4FFFF" class="bodyText2"><strong>Reference/Citation</strong>:<br>
                (a) <em>The datafile: please cite as follows:</em><br>
                Judith N. Lund, Elizabeth A. Josephson, Randall R. Reeves, Tim D. Smith, 'American Offshore Whaling Voyages: A Database'
                in M.G Barnard  &amp; J.H Nicholls (comp.) <em>HMAP Data Pages</em> (www.hull.ac.uk/hmap)<br>
                (b) <em>Supporting documentation: please cite as follows:</em><br>
              Judith N. Lund, Elizabeth A. Josephson, Randall R. Reeves, Tim D. Smith, ‘American Offshore Whaling Voyages: A Database’, Supporting  Documentation, in M.G Barnard                &amp; J.H Nicholls (comp.) <em>HMAP Data Pages</em> (www.hull.ac.uk/hmap)</td>
            </tr>
            <tr>
              <td colspan="6" class="bodyText2">&nbsp;</td>
            </tr>
          </tbody>
          </table>

          <h2>Data Tools: Click on a tool's icon to be re-directed to a download</h2>
          <table><tbody><tr>
              <td width="80%" height="78" bgcolor="#CECECE"><strong>PHP</strong> (Pre-Hypertext Processor) is a public domain,  freeware high level programming language (4GL) employed in                 linking HTML code to online databases.</td>
              <td width="20%"><div align="right"><a href="http://www.php.net" target="_blank"><img src="assets/img/php.gif" alt="PHP Logo" width="120" height="67" border="0"></a></div></td>
				</tr>

            <tr>
              <td height="78" bgcolor="#CECECE"><strong>MySQL</strong> (My Structured Query Language) is a public domain,  freeware Enterprise Database Management System employed for creating                 and querying databases. </td>
              <td><div align="right"><a href="http://www.mysql.com/" target="_blank"><img src="assets/img/mysqllogo.gif" alt="MySQL Logo" width="90" height="52" border="0"></a></div></td>
            </tr>
            <tr>
              <td height="78" bgcolor="#CECECE"><strong>Adobe Reader</strong> enables reading of <strong>PDF</strong> documents that are generally available on the web. </td>
              <td><div align="right"><a href="http://www.adobe.com/products/acrobat/readstep2.html" target="_blank"><img src="assets/img/reader_icon_special.jpg" alt="Adobe Reader logo" width="125" height="104" border="0"></a></div></td>
            </tr>
            <tr>
              <td height="78" bgcolor="#CECECE"><strong>7-Zip</strong> enables unzipping of compressed files in various formats, especially WinZip and PKZip files. </td>
              <td><div align="right"><a href="http://www.7-zip.org/" target="_blank"><img src="assets/img/7ziplogo.gif" alt="7-zip" width="110" height="63" border="0"></a></div></td>
            </tr>
          </tbody></table>
