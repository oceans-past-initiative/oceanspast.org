<html>
<title>OPI.org</title>
<?php
if (file_exists('local.txt')) {
    //don't load admin headers
} else {
    // we are in production server
    include "login/misc/pagehead.php";
} ?>
<?php include 'header.php';?>
<body>
  <?php include 'nav.php';?>

<div id="container">
    <main>
      <aside class="left">
        <?php include 'about_left.php';?>
      </aside>
      <aside class="right">
        <div class="agenda">
          <div class="project article">
            <div id="steering committee">
              <h1>Steering Committee</h1>
              <p>On this page you can learn about our current Steering Committee and what they do at OPI.</p><br>
              <img class="in-body" src="assets/img/board/Board2019/Group_Bremerhaven.jpg">
              <figcaption>the 2019 board in Bremerhaven</figcaption>


              <p><strong><h2>Poul Holm, Trinity College Dublin, Ireland.</h2></strong>
              <img class="thumb" src="assets/img/board/Board2019/Poul_Holm.jpg">
              <header>As a professor of environmental history, my field is human-nature relations. My current research focusses on the North Atlantic marine environment, c. 1400-1700, especially the expansion of the fisheries around 1500 during a time of climate change and internationalisation of trade.</header>
              <p>
                <b>— What is your position on the Board and what does this entail?</b><br>
                I serve as the chair of the Board.<br>
                <b>— What is your interest in serving on the Board of OPI?</b><br>
                I am keen to secure the long-term sustainability of OPI. We are a small but growing community and it is vital we maintain a welcoming and well-functioning umbrella organisation.<br>
                <b>— What is one hope or goal you have for OPI moving forward?</b><br>
                I hope we can keep uncovering amazing stories. Marine environmental history and historical ecology was borne out of curiosity: Can we know what the seas used to look like 500 years ago? What was the role of the sea through human history? Caring about a marine world that we hardly ever see must build on a sense of connectedness. Our kind of history feeds that basic human need.
              </p>

              <hr />

              <h2>Alison MacDiarmid, National Institute of Atmospheric and Water Research (NIWA), New Zealand.</h2>
              <img class="thumb" src="assets/img/board/Board2019/Alison_MacDiarmaid.jpg">
              <p>
                <b>— What is your position on the Board and what does this entail?</b><br>
                Board Member. As well as contributing to regular monthly OPI business, I have co-convened the last three Oceans Past conferences in Estonia, Portugal, and Germany.<br>
                <b>— What is your interest in serving on the Board of OPI?</b><br>
                My interest in serving on the Board is to bring a southern hemisphere and Oceania perspective to OPI which continues to have primarily a European and North American makeup. It would be great to broaden OPI membership and participation in OP conferences to Asian, African and South American researchers.<br>
                <b>— What is one hope or goal you have for OPI moving forward?</b><br>
                My hope is for OPI to help ensure that a historical perspective is included in marine policy development so that, for example, goals for rebuilding ecosystems are not restricted to the recently observed past. This means that we need to ensure that implications for policy development are spelt out clearly in our reports and papers in language that policy makers can readily adopt. This could require us to invite marine policy analysts to be co-authors, or better still collaborators in our research from first conceptualisation. I know from first-hand experience in New Zealand that policy makers welcome such efforts.
              </p>

              <hr />

              <h2>Cristina Brito, Assistant Professor, NOVA FCSH, CHAM - Centre for the Humanities (Portugal).</h2>
              <img class="thumb" src="assets/img/board/Board2019/Cristina_Brito.jpg">
              <header>I am based in Lisbon where I conduct my research on early modern marine environmental history, focusing on the study of manatees, sea turtles and many other large marine animals, and the relationships between humans and the non-human marine world.</header>
              <p>
                <b>— What is your position on the Board and what does this entail?</b><br>
                I am the Treasurer in the OPI Board. So far, I have been the secretary and giving support to the OPI Newsletter, and I am currently running the finances alongside other members of the Board and an external auditor.<br>
                <b>— What is your interest in serving on the Board of OPI?</b><br>
                My main goal in this group is to make sure that the Portuguese and Spanish-speaking scientific communities are represented, mostly by making an effort in the integration of colleagues and scholarship from the South Atlantic (African and Central and South American countries).<br>
                <b>— What is one hope or goal you have for OPI moving forward?</b><br>
                My hope is for OPI to become a global platform for the dissemination and outreach of matters related to the Past of the Oceans in the broad sense, particularly in places and communities where these themes are not known; to engage the academic communities in developing new disciplines and fields of research, such as marine environmental history and historical marine ecology; to expand the value of history in the understanding of current day issues/problems and in the proposal of sustainable management measures for endangered species, and for marine ecosystems and resources. And, on a more personal note, to keep on trying to save the whales!
              </p>

              <hr />

              <h2>Dr. Gesche Krause, Alfred Wegener Institute Helmholtz Center for Polar and Marine Research, Bremerhaven (Germany).</h2>
              <img class="thumb" src="assets/img/board/Board2019/Gesche_Krause.jpg">
              <header>As marine social scientist, I am working at the science-society interface, studying how and in what ways different realms of knowledge can be identified, connected, and analyzed to harness transdisciplinary research approaches.</header>
              <p>
                <b>— What is your position on the Board and what does this entail?</b><br>
                I am member of the board, “representing” the linkages to the different contemporary science realms discourses.<br>
                <b>— What is your interest in serving on the Board of OPI?</b><br>
                I want to support the highly interdisciplinary setting and nature of OPI, which appeals greatly to me, and thus strive to make OPI a point of entry for all people that have interest in the broad, global, interrelated, interconnected and systemic perspectives of human-nature relationships through deep time.<br>
                <b>— What is one hope or goal you have for OPI moving forward?</b><br>
                Harnessing OPI as vehicle to break down common distinctions between people and their “environment” by embracing and highlighting the deep-time impacts of societal actions and political decisions vis à vis natural processes and their dynamics. By this, we improve our knowledge on how and in what ways to link current policies with what past generations have done and what future generations will be able to do.
              </p>

              <hr />

              <h2>Dr. Ruth Thurstan, Lecturer in Biosciences at the Centre for Ecology and Conservation, University of Exeter’s Cornwall campus.</h2>
		          <img class="thumb" src="assets/img/board/Board2019/Ruth_Thurstan.jpg">
              <header>My core area of research is marine historical ecology, with a particular focus upon understanding the scale of ecological and social changes in fisheries over the past 200 years.</header>
              <p>
                <b>— What is your position on Board and what does this entail?</b><br>
                I am the Secretary of the Board, and will also be involved (with many others) in convening the 2020 Oceans Past conference.<br>
                <b>— What is your interest in serving on the Board of OPI?</b><br>
                I am interested in examining the magnitude and drivers of social-ecological change in marine systems, which requires interdisciplinary thinking and collaboration with researchers and stakeholders. By being on the Board I hope to be in a better position to engage researchers, students, and the wider public in such research.<br>
                <b>— What is one hope or goal you have for OPI moving forward?</b><br>
                With Dr. Emily Klein, I co-chair the ICES Working Group on the History of Fish and Fisheries (WGHIST), which provides a link between historical ecology findings and fisheries scientists, managers and stakeholders. I hope that we can use this channel to better connect our findings to management and policy and communicate to as many people as possible the changes that have occurred to our oceans as a result of human impacts.
              </p>

              <hr />

              <h2>Dr. Ben Fitzhugh, Professor of Anthropology (specializing in Archaeology), University of Washington (USA).</h2>
	            <img class="thumb" src="assets/img/board/Board2019/Ben_Fitzhugh.jpg">
              <header>I also serve as the current director of the Quaternary Research Center at the University of Washington, and my scholarship focuses on human-environmental dynamics in maritime, Subarctic regions around the North Pacific over the past several thousand years, with particular emphasis on the indigenous cultures of the Kodiak and Kuril Archipelagos.</header>
              <p>
                <b>— What is your position on the Board and what does this entail?</b><br>
                I am a new member on the OPI and of the OPI Board. I will be co-convening the 2020 OPI meeting along with Ruth Thurstan and others.<br>
                <b>— What is your interest in serving on the Board of OPI?</b><br>
                I agreed to serve out of a deep interest in the dynamic interplay between the ocean systems and the histories of maritime cultures, not to mention the opportunity to become acquainted with an exciting community of scholars with interests closely connected to my own. In 2014, I co-founded the Paleoecology of Subarctic Seas (PESAS) working group, a science-heavy, interdisciplinary community within the fisheries ecology research group known as the Ecosystem Studies of Subarctic Seas, and PESAS has always wanted to have more robust connections with the environmental humanities and maritime history. OPI has been doing this well and joining forces seems like productive fun!<br>
                <b>— What is one hope or goal you have for OPI moving forward?</b><br>
                I see OPI as the perfect venue to build richer connections between the histories of human maritime interactions and the temporal dynamics of climate and ocean ecosystems. I am looking forward to learning more about the great work being done in OPI to connect these themes, especially in the European North Atlantic area, and to promote similar engagement in other regions of the world. One area for expanding is in the North Pacific, where my own research is focused and where we have networks connecting North American and East Asian scholars. I believe historical ecology of the maritime system has vital lessons to share with the world about long-term trends in human-ecological relationships that are obscured in much contemporary the science and policy, driven by less than 100 years of environmental data and a lack of focus on connected human histories. We can do better, and our stories are important for future planning.
              </p>
            </div>
          </div>
        </div>
      </aside>
    </main>

  </div>
  <?php include 'footer.php';?>
</body>
</html>
