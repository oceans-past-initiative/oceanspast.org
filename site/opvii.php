<html>
<title>OPI VII conference</title>
<meta property="og:title" content="OPI VII conference"/>

  <?php
  if (file_exists('local.txt')) {
      //don't load admin headers
  } else {
      // we are in production server
      include "login/misc/pagehead.php";
  } ?>
  <?php include 'header.php';?>
<body>
  <?php include 'nav.php';?>

<div id="container">
    <main>
      <aside class="left">
        <?php include 'conferences_left.php';?>
      </aside>
      <aside class="right">
        <div class="agenda">
          <div class="project article">
            <div id="about">

              <h1>Oceans Past VII Conference</h1>
              <h4>Tracing human interactions with marine ecosystems through deep time: Implications for policy & management</h4>
				<h5>Bremerhaven, Germany - 22-26 October 2018</h5>
				
				<p><i>Unsolicited feedback from participants indicates that this latest Oceans Past conference was a great success; one experienced conference goer reported “this has been, without, any doubt, one of the best meetings I have ever attended”.</i></p>


              <img src="assets/img/OPVII_lecture.jpg" class="img-responsive" width="200" height="300" align="right" alt=""/>

              <h5>Official Conference Website: <a href="https://www.awi.de/OPP7">https://www.awi.de/OPP7</a></h5>

             <p>The Oceans Past conferences are a platform for dissemination and discussion of new research findings in the fields of historical marine ecology, and fisheries and maritime history. The latest conference was held at the <b>Alfred Wegener Institute Helmholtz Centre for Polar & Marine Research (AWI)</b> and the <b>German Maritime Museum (DSM)</b>, and was the seventh event since 2005. It was co-convened by <b>Alison MacDiarmid</b> (New Zealand) and <b>Poul Holm</b> (Ireland), superbly organized locally by <b>Gesche Krause</b> (Germany) and her hard-working team, and supported by <b>Henn Ojaveer</b> (Estonia), who chaired the Science Steering Group, and <b>Joanne D’Arcy</b> (Ireland). OPVII was financially supported by the <b>EU COST Action Oceans Past Platform (OPP)</b>, coordinated by Dr. Holm.</p>

              <p>The highly interdisciplinary conference had over 100 registered participants comprising both natural and social scientists (ecologists, oceanographers, economists, historians, archaeologists) from more than 25 countries. During the conference, 75 talks, 8 posters and four key-note addresses were presented, with talks arranged in the following back-to-back sessions, allowing every participant to enjoy each of the papers:</p>

              <p><li>- Session I – Oceans prior to contemporary exploitation</li>
				<li>- Sessions II & III - Drivers of environmental use and change across historical time frames</li>
				 <li>- Sessions IV-VI – The significance of marine resources for human societies over time</li>
				 <li>- Sessions VII-IX – Paleoecology of the Subarctic Seas: High Latitude Climates, Oceans, Ecosystems, and Human Histories</li>
				  <li>- Sessions X-XII – Implications of past and present human ocean activities for coastal and marine policy development</li> 
				  <li>- Session XIII – Development of indictors</li>
				  <li>- Sessions XIV & XV – Factors that have encouraged societies to exploit or leave the oceanss</li>
				</p>
<figure align="right"><figcaption><img src="assets/img/opvii_summary.jpg">Slide from paper by Hreiðar Þór Valtýsson (hreidar@unak.is) on “The fishing
effort by the Icelandic fleet since 1900”</figcaption></figure>
              <p>In addition, topical breakout groups engaged all conference participants on a diverse range of questions, including: How do we connect historical data sources to contemporary management and policy needs (“Connections”)?; Are historical sources and study as well as its relevant and potential applicability known to decision makers for current policy and management? (“Barriers”); How does uncertainty in historical studies or sources differ from or overlap with current sources and studies used for management advice? (“Uncertainty”); In what ways can we interact with other disciplines to elevate the importance of approaches in marine historical ecology, etc. for current data-poor fisheries? (“Cross-scales and Interdisciplinarity”); What are the new and developing approaches in historical marine ecology, such as the use of ancient DNA, stable isotopes, etc? (“Emerging Trends”). A Rapporteur Session on the fifth day shared insight from breakout groups with all conference participants.</p>
              <ul>
          <figure> <figcaption><img src="assets/img/opvii_dinner.jpg" align="left">Dinner is served!</figcaption></figure> <p> The conference had strong participation and collaboration with members
of International Council for the Exploration of the Seas (ICES) Working
Group on the History of Fish and Fisheries (WGHIST) and Working Group
of Social and Economic Dimensions of Aquaculture (WGSEDA), the
Paleoecology of Subarctic Seas (PESAS) Working Group (member of
Ecosystem Studies of Subarctic and Arctic Seas/Integrated Marine
Biosphere Research), and members of the EU-COST Action Oceans Past
Platform (OPP). The next Oceans Past will be held in Bruge, Belgium in 2020.</p>

            </div>
          </div>
        </div>
      </aside>

    </main>

  </div>
  <?php include 'footer.php';?>
</body>
