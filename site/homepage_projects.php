<div class="agenda">
  <div class="project presentation">
    <div class="project-detail-header">
      <p class="section-title">Ongoing projects</p>
      <br>
      <h3 class="project-title"><a href="#">ICES WGHIST</a></h3>
      <div class="height-limited">
        <p class="project-exerpt">The Oceans Past Initiative is closely linked with the expert working group, WGHIST of the International Council for the Exploration of the Sea (ICES): The ICES Working Group on the History of Fish and Fisheries (WGHIST) brings together fisheries scientists, historians, and marine biologists working on multidecadal to centennial changes in the marine environment. WGHIST aims to improve the understanding of the long-term dynamics of fish populations, fishing fleets, and catching technologies. The results are used for setting baselines for management, restoration, and conservation of marine resources and ecosystems.</p>
        <p class="read-more"><a href="#" class="read-more-button"><i class="fa fa-chevron-down" aria-hidden="true"></i></a></p>
      </div>
    </div>
  </div>
  <div class="project presentation">
    <div class="project-detail-header">
      <h3 class="project-title">EU COST Action Network</h3>
      <div class="height-limited">
        <p><img class="project-hero" src="assets/img/hmap.jpg"></p>
        <p class="project-exerpt">The Oceans Past Initiative is closely linked with the expert working group, WGHIST of the International Council for the Exploration of the Sea (ICES): The ICES Working Group on the History of Fish and Fisheries (WGHIST) brings together fisheries scientists, historians, and marine biologists working on multidecadal to centennial changes in the marine environment. WGHIST aims to improve the understanding of the long-term dynamics of fish populations, fishing fleets, and catching technologies. The results are used for setting baselines for management, restoration, and conservation of marine resources and ecosystems.</p>
        <p class="read-more"><a href="#" class="read-more-button"><i class="fa fa-chevron-down" aria-hidden="true"></i></a></p>
      </div>
    </div>
  </div>
 
</div>
