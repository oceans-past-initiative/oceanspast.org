<html>
<title>OPI GDPR</title>
<meta property="og:title" content="OPI GDPR"/>
<?php
if (file_exists('local.txt')) {
    //don't load admin headers
} else {
    // we are in production server
    include "login/misc/pagehead.php";
} ?>
  <?php include 'header.php';?>
<body>
  <?php include 'nav.php';?>

<div id="container">
    <main>
      <aside class="left">
        <?php include 'conferences_left.php';?>
      </aside>
      <aside class="right">
        <div class="agenda">
          <div class="project article">
            <div id="constitution">
              <small>The following applies primarily to visitors that hold a oceanspast.org user account. <br><a href="#cookies">Scroll down to read about how we use cookies.</a></small>
              <h1><strong>OPI</strong> <strong>Data Privacy Notice</strong></h1>
              <h2><em>April 2019 </em></h2>
              <p>The Oceans Past Initiative recognizes the  importance of privacy of its members and viewers. Our approach is to ensure that  all reasonable efforts are made to address protections required by the General  Data Protection Regulation (GDPR).<br />
                Where relevant, we use your information for the  legitimate purpose of holding valid Membership lists pertaining to the OPI and  disseminating news and information to our Members.</p>
                <h2>The data we hold  on you: </h2>
                <ul>
                  <li>Your title, first and last name</li>
                  <li>Your email address(es)</li>
                  <li>Your Institution(s)</li>
                  <li>Your Biography – where appropriate</li>
                  <li>Your Image – where appropriate<br />
                  </li>
                </ul>
                <p>This data will be held for a period of up to 5  years following the last year of membership, unless a written request is  received to remove your data from our records.
                </p>

                <h2>We use this data to: </h2>
                <ul>
                  <li>
                    Keep an accurate and up to date record of OPI  Members.
                  </p>
                </li>
              </ul>
              <h2>What we do NOT do  with the data is:  </h2>
              <ul>
                <li>  Sell or rent your data to any third parties</li>
                <li>Pass on your information to any third parties  without your clear, written consent </li>
              </ul>
              <h2>Published materials</h2>
              <p>The OPI complies with a general Public Domain  policy in relation to its online published materials (including documents,  newsletters, images, data series, databases and information); however, where Rights  reside in the event of Personal Property, or Copyright, all materials are  deemed to be covered under Creative Commons Licence: Attribution-NonCommercial  4.0 International (CC BY-NC 4.0) <a href="https://creativecommons.org/licenses/by-nc/4.0/legalcode">https://creativecommons.org/  licenses/by-nc/4.0/legalcode</a>.</p>
              <p>This licensing is notwithstanding any specific  licensing agreement that the OPI, a Member or Members, in respect of Personal  Property or Copyright, may stipulate as an alternative.
              </p>
              <h2>International Rights</h2>
              <p>While the GDPR is applicable under European Union Law (25 May 2018), the OPI shall maintain a standard of General Data Protection that adheres to these statutes wherever practicable and reasonable outside of the European Union.</p>
              <h2>Your rights </h2>
              <p>The GDPR allows you to request access, corrections and removal of your personal information. To avail yourself of  this, please email us at <a href="mailto:breenri@tcd.ie">breenri@tcd.ie</a> or <a href="mailto:nichollj@tcd.ie">nichollj@tcd.ie</a>.</p>
              <br>
              <p>The following applies to all visitors, account holders and normal visitors.</p>
              <hr>
              <h1 id="cookies">Cookie usage</h1>
              <h2>How this site uses cookies</h2>
              <p>This website uses cookies for trafic analytics and user management, if you do not wish your visit to be logged, we encourage you to <a target="_blank" href="">set your browser to 'do not track'</a> which will disable all but one cookie for this site. In this case, the remaining cookie remembers that you have acknoledged this policy.</p>
              <div id="dnt-status"></div>
            </div>
          </div>
        </div>
        </aside>

    </main>

  </div>
  <?php include 'footer.php';?>
</body>
