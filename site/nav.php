<aside class="top">
  <div>
  <a class="home" href="index.php"><img alt="Oceans Past Initiative Logo" class="opi" src="assets/img/OPI.svg"></a>

  <nav id="menu" class="nav-collapse"><a href="#">
          <ul>
            <li class="head-li"><a href="about.php">about</a></li>
            <li class="secondaryitem"><a href="about.php">about</a></li>
            <li class="secondaryitem"><a href="steering_committee.php">steering committee</a></li>
            <li class="secondaryitem"><a href="constitution.php">constitution</a></li>
            <li class="secondaryitem"><a href="institutions.php">institutions and supporting partners</a></li>
          </ul>
          <ul>
            <li class="head-li"><a href="projects.php">projects</a></li>
            <li class="secondaryitem"> <a href="projects.php#ICES"> ICES WGHIST</a></li>
            <li class="secondaryitem"> <a href="projects.php#publications"> publications</a></li>
            <li class="secondaryitem structuralli">finished projects:</li>
            <li class="secondaryitem"> <a href="projects.php#EU-COST"> EU COST Action Network</a></li>
            <li class="secondaryitem"> <a href="projects.php#OPP"> OPP</a></li>
            <li class="secondaryitem"> <a href="projects.php#HMAP-db"> HMAP DBs</a></li>
          </ul>
          <ul>
            <li class="head-li"> <a href="conferences.php"> conferences</a></li>
            <li class="secondaryitem"><a href="opviii.php">Oceans Past VIII (2020)</a></li>
            <li class="secondaryitem"><a href="opvii.php">Oceans Past VII</a></li>
            <li class="secondaryitem"><a href="conferences.php">Access conference archive</a></li>
          </ul>
          <ul>
            <li class="head-li"><a href="">news</a></li>
            <li class="secondaryitem"><a href="#">all events</a></li>
            <li class="secondaryitem"><a href="#">newsletter archive</a></li>
          </ul>
          <ul class="secondary">
            <li class="secondary head-li"><a title="contact" class="contact-button" href="contact.php"><i class="fa fa-envelope" aria-hidden="true"></i><span> contact</span></a></li>
            <?php
            if (file_exists('local.txt')) {
                echo '<li class="secondary head-li"><a title="this button becomes active in production" class="login-button button" href="#"><i class="fa fa-user-circle" aria-hidden="true"></i><span> login to opi.org</span></a></li>';
            } else {
                if ($auth->isLoggedIn()) {
                  echo '<li class="secondary head-li"><a title="To user dashboard" class="login-button button" href="/admin/"><i class="fa fa-user-circle" aria-hidden="true"></i><span>user: ';
                  echo $_SESSION['username'];
                  echo '</span></a></li>';
                } else {
                  echo '<li class="secondary head-li"><a title="login" class="login-button button" href="/login/"><i class="fa fa-user-circle" aria-hidden="true"></i><span> login to opi.org</span></a></li>';
                }
            } ?>
            <li class="secondary head-li" style=""><a title="twitter" class="contact-button" href="https://twitter.com/oceans_past"><i class="fa keep fa-twitter" aria-hidden="true"></i></a></li>
            <p class="menu-footer">the OPI is supported by <a href="institutions.php">many institutions and supporting parties</a><br><br><a href="GDPR.php">access the OPI GDPR policy</a> </p>
          </ul>

  </a></nav>
  </div>
</aside>
