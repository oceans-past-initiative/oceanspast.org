<div class="agenda">
  <div class="project article">
    <div id="about">
      <h2>About OPI</h2>
      <img class="thumb" src="assets/img/image1.jpg" alt="image of painting of boat in ocean">
      <p>The Oceans Past Initiative (OPI) is a global research network for marine historical research. Our goal is to enhance knowledge and understanding of how the diversity, distribution and abundance of marine life in the world’s oceans has changed over the long term to better indicate future changes and possibilities. OPI welcomes anyone interested in the history of humankind’s interactions with life in the oceans including paleo-ecologists and climatologists, archaeologists, marine environmental historians, economic historians, oral historians, historical ecologists, fisheries historians, and marine environmental and fisheries policy makers and managers.</p>
      <p>read the <a href="constitution.php">OPI constitution</a> documents</p>
    </div>
    <!-- <div id="board">
      <h2>Steering Committee & Executive Board</h2>
      <p>The network will commit itself to coordinate resources and provide useful information to the marine historical research community, such as by circulating news about grant calls and funding opportunities, information on recent papers in the field, announce successful projects, and distribute information for students on courses related to marine historical research.It is planned for OPI to become a venue where researchers worldwide interested in historical studies could virtually meet and discuss relevant issues globally. OPI can provide an umbrella, under which already completed, currently running, and also planned projects and initiatives can be linked, and make their results available for decision-makers and the interested public. <a href="steering_committee.php">Click here to view the committee members</a></p>
    </div> -->
    <div id="steering-committee"></div>
    <div id="curiosity">
      <h2>Scientific Curiosity</h2>
      <img class="in-article-img" src="assets/img/OPI2015ScientificCuriosityPicture2.jpg" alt="image of painting of cooked fish">
      <p>Historical research is playing an increasingly important role in marine sciences. Historical data are also used in policy making and marine resource management, and have helped to address the issue of shifting baselines for numerous species and ecosystems. Although many important research questions still remain unanswered, tremendous developments in conceptual and methodological approaches are expected to contribute to a comprehensive understanding of the global history of human interactions with life in the seas. Based on our experiences and knowledge from the “History of Marine Animal Populations” project, the Oceans Past Initiative (OPI) has been established to assist in bringing together and connecting researchers interested in marine historical studies worldwide.</p>
    </div>
  </div>
</div>
