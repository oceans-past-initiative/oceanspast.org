<div class="agenda">
  <div class="project article">
    <div id="general-contact">
      <h2>Contacting the OPI</h2>
      <p>There are many ways in which you can communicate and engage with the OPI, all of which are listed below.</p>
    </div>
    <div id="user-accounts">
      <h2>User accounts</h2>
      <p>This website contains a user area that can give you access to internal documents, databases and newsletter archives. <a href="/login/signup.php">Create an account here.</a></p>
    </div>
    <div id="newsletter">
      <h2>Newsletter</h2>
      <p><a title="contact" href=""><i class="fa fa-envelope" aria-hidden="true"></i> Click here to receive the trimestrial newsletter.</a> To access the newsletter archive, you must <a href="/login">log in</a> or <a href="/login/signup.php"> create an account</a>.</p>
    </div>
    <div id="mailing-lists">
      <h2>Mailinglists</h2>
      <p>The OPI also hosts mailing lists for internal contact, research updates and more. Find out about the various mailing lists <a href="http://staging.oceanspast.org/login/index.php?refurl=http%3A%2F%2Fstaging.oceanspast.org%2Fpage_2.php"> in the members area.</a></p>
    </div>

  <div id="sm">
    <h2>Social media</h2>
    <a title="twitter" href="https://twitter.com/oceans_past"><i class="fa keep fa-twitter" aria-hidden="true"></i> The OPI on Twitter</a>
  </div>
  <div id="direct-enquiries">
    <h2>Direct enquiries</h2>
    <p>You may also email <a href="mailto:info@oceanspast.org">info@oceanspast.org</a>for general enquiries.</p>
  </div>
  </div>
</div>
