<html>
<title>OPI VI conference</title>
<meta property="og:title" content="OPI VI conference"/>
<?php
if (file_exists('local.txt')) {
    //don't load admin headers
} else {
    // we are in production server
    include "login/misc/pagehead.php";
} ?>
  <?php include 'header.php';?>
<body>
  <?php include 'nav.php';?>

<div id="container">
    <main>
      <aside class="left">
        <?php include 'conferences_left.php';?>
      </aside>
      <aside class="right">
        <div class="agenda">
          <div class="project article">
            <div id="about">

              <h1>Oceans Past VI Conference</h1>
              <h2><i>Historical Perspectives on the Elements and Dynamics of the Marine Socio-Ecological System</i></h2>
              <h3>16-18 May 2017 | Sesimbra, Portugal</h3>

              More info: <a href="http://www.escolademar.pt/oceanspastvi/">http://www.escolademar.pt/oceanspastvi/</a> <br><br>

              <p>Oceans Past VII will be a celebration of Oceans Past like never before. You will meet colleagues from around the world, have direct access to politicians and managers, and enjoy special sessions, mini-symposiums, and exhibitions. Come join us for a defining conference!</p>

              <p><b> Call for papers:</b> The submission of oral and poster papers encompassing, but not limited to, the following topics is encouraged: Meanwhile, in what could be dubbed a sea change of history, environmental historians are also studying human engagement with the underwater realm. Historians, including archaeologists, sociologists, and geographers, have engaged with marine scientists in an interdisciplinary effort to bring together the long-term study of human and animal worlds. Thanks to this collaborative effort, researchers have not only identified, but in many cases resolved, the problem of the &lsquo;shifting baseline&rsquo; by pushing back the chronological limits of our knowledge.</p>

              <p>Call for papers: The submission of oral and poster papers encompassing, but not limited to, the following topics is encouraged:</p>
              <ul>
                <li> ❖ Oceans prior to contemporary exploitation</li>
                <li> ❖ Drivers of environmental use and change across historical time frames</li>
                <li> ❖ The significance of marine resources for human societies over time</li>
                <li> ❖ Factors that have encouraged societies to exploit or leave the oceans</li>
                <li> ❖ Development of indictors</li>
                <li> ❖ Implications of past and present human ocean activities for coastal and marine policy development</li>
              </ul>

              <h2><a href="https://www.tcd.ie/history/opi/assets/pdf/FlyerOceansPastConference2017_FINAL_31.03.pdf"><strong>Conference Programme</strong></a></h2>
              <h2><strong><a href="http://www.escolademar.pt/wp-content/uploads/Oceans-Past-VI-Conference-Abstract-Book.pdf">Conference Abstracts</a></strong><a href="http://vbn.aau.dk/files/211107389/Oceans_Past_V_Abstracts.pdf"></a></h2>

              <h3>Funding</h3>
              <ul>
                <li><a href="http://www.cm-sesimbra.pt/"><strong>Municipality of Sesimbra&nbsp;</strong></a></li>
                <li><a href="http://oceanspast.org/cham.fcsh.unl.pt"><strong>CHAM - Portuguese Center for Global History</strong></a></li>
                <li><a href="http://oceanspast.org/sciencesmar.wixsite.com/association"><strong>APCM - Sea Sciences Association</strong></a></li>
              </ul>

              <h2>Oceans Past Platform</h2>

              <h3>Working Group Meetings</h3>

              <p>OPI is connected with the COST Action Oceans Past Platform (2015-2018) that aims to determine the socio-economic, political, cultural and ecological implications of human impacts on marine life, and to develop historical descriptors and indicators for marine and coastal management. Meetings to discuss the current research on the five OPP Working Groups will be held in Sesimbra at May 19th 2017. https://www.tcd.ie/history/opp/ </p>
              <p>The action calls on historians, archaeologists and social scientists as well as colleagues from the marine sciences to engage in dialogue and collaboration with ocean and coastal managers. Researchers gather within five thematic Working Groups , namely 1) Fish production, 2) Coastal settlements, 3) Aquaculture, 4) Changing values, and 5) Gendered seas.</p>

              <h2>Convention</h2>

              <p>Alison MacDiarmid (New Zealand) - alison.macdiarmid@niwa.co.nz</p>

              <h2>Scientific Steering Committee</h2>

              <li>Henn Ojaveer (Estonia, chair)</li>
              <li>Poul Holm (Ireland)</li>
              <li>Malcolm Tull (Australia)</li>
              <li>Patricia Miloslavich (Australia)</li>
              <li>Lembi Lõugas (Estonia)</li>
              <li>Gesche Krause (Germany)</li>
              <li>Bao Maohong (China)</li>

              <h2>Local Organising Committee</h2>

              <li>Cristina Brito (Chair) - escolademar@gmail.com (CHAM)</li>
              <li>Nina Vieira - ninavieira.pt@gmail.com (CHAM)</li>
              <li>Catarina Garcia - catarinagarcia@gmail.com (CHAM)</li>
              <li>Inês Carvalho - carvalho.inesc@gmail.com (APCM)</li>
              <li>Joanne D'Arcy - darcyjo@tcd.ie (OPP - TCD)</li>

            </div>
          </div>
        </div>
      </aside>

    </main>

  </div>
  <?php include 'footer.php';?>
</body>
