<html>
<title>OPI conferences</title>
<meta property="og:title" content="OPI conferences"/>
<?php
if (file_exists('local.txt')) {
    //don't load admin headers
} else {
    // we are in production server
    include "login/misc/pagehead.php";
} ?>
  <?php include 'header.php';?>
<body>
  <?php include 'nav.php';?>

<div id="container">
    <main>
      <aside class="left">
        <?php include 'conferences_left.php';?>
      </aside>
      <aside class="right">
        <?php include 'conferences_body.php';?>
      </aside>

    </main>

  </div>
  <?php include 'footer.php';?>
</body>
