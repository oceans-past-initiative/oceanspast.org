<script type="application/javascript" src="assets/js/fastclick.js"></script>
<script type="application/javascript" src="assets/js/scroll.js"></script>
<script type="application/javascript" src="assets/js/fixed-responsive-nav.js"></script>
<script type="application/javascript" src="assets/js/toc.min.js"></script>


<div id="myModal" class="modal fade in" tabindex="-1" role="dialog" data-keyboard="false" data-backdrop="false">
  <div class="modal-dialog modal-dialog-centered">
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title" id="exampleModalLabel">Cookies and GDPR on oceanspast.org</h4>
      </div>
      <div class="modal-body">
        <small>This website uses as few cookies as possible and we thoughoughly believe in respecting your online privacy. Please read our <a target="_blank" href="GDPR.php">cookie and data protection statements</a> and click the button below to agree.</small>
        <small>
          <hr>
          <div id="dnt-status">
          </div>
        </small>
      </div>

      <div class="modal-footer">
        <button type="button" id="myModalClose" class="btn btn-secondary close" data-dismiss="modal" data-target="#myModal">Agree</button>
      </div>
    </div>
  </div>
</div>



<!-- <script type="text/javascript">
// $( "nav.closed" ).click(function() {
//   console.log("hey");
//   $( this ).slideToggle( "opened" );
// });
$('#menu').click(function () {
    $('nav.closed').slideToggle("clo");
    $('.secondaryitem').slideToggle();
}
);

</script> -->

<script type="text/javascript">
  $( ".read-more-button" ).click(function() {
    var parentDiv = $(this).parent().closest('div');
    parentDiv.toggleClass("height-unlimited height-limited");
    if (parentDiv.hasClass("height-unlimited")) {
        $(this).html('<i class="fa fa-chevron-up" aria-hidden="true"></i>');
    } else {
        $(this).html('<i class="fa fa-chevron-down" aria-hidden="true"></i>');
    };
  });


  $('#toc').toc({
    'selectors': 'h1,h2,h3', //elements to use as headings
    'container': 'body', //element to find all selectors in
    'smoothScrolling': true, //enable or disable smooth scrolling on click
    'prefix': 'toc', //prefix for anchor tags and class names
    'onHighlight': function(el) {}, //called when a new section is highlighted
    'highlightOnScroll': true, //add class to heading that is currently in focus
    'highlightOffset': 100, //offset to trigger the next headline
    'anchorName': function(i, heading, prefix) { //custom function for anchor name
        return prefix+i;
    },
    'headerText': function(i, heading, $heading) { //custom function building the header-item text
        return $heading.text();
    },
    'itemClass': function(i, heading, $heading, prefix) { // custom function for item class
      return $heading[0].tagName.toLowerCase();
    }
  });

  $(document).ready(function () {
      //if cookie hasn't been set...
      if (document.cookie.indexOf("CookiePolicyAccepted=true")<0) {
          $("#myModal").modal("show");
          //Modal has been shown, now set a cookie so it never comes back
          $("#myModalClose").click(function () {
              $("#myModal").modal("hide");
          });
          document.cookie = "CookiePolicyAccepted=true; expires=Fri, 31 Dec 9999 23:59:59 GMT; path=/";
      };
      if (navigator.doNotTrack == 1) {
          var dntStatus = "<br><button type='button' class='btn btn-outline-success'>Your browser is set to : <b>Do not track</b></button>";
          document.getElementById("dnt-status").innerHTML = dntStatus;
      } else {
          var dntStatus = "<br><button type='button' class='btn btn-outline-info'>Your browser currently <b>allows</b> tracking</button>";
          document.getElementById("dnt-status").innerHTML = dntStatus;
      };
    });

</script>
