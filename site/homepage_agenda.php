<div class="content">
<p class="section-title">news & updates</p>
  <section>
    <p class="project-title">Next Oceans Past Conference</p>
    <p class="project-exerpt">After the terrific success of OPVII in Bremerhaven, he Steering Committee is delighted to announce that the next Oceans Past Conference will take place in Bruges Belgium in 2020. </p>
    <p class="read-more-link"><a href="opviii.php">→ read more</a></p>
  </section>
  <section>
    <p class="project-title">Request to Join OPI today!</p>
    <p class="project-exerpt">Want to know more about and how to join OPI? Just send an email to info@oceanspast.org today.</p>
    <p class="read-more-link"><a href="about.php">→ read more</a></p>
  </section>
</div>
