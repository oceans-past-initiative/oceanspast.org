# Oceanspast.org

Website refresh

current staging site: <http://oceanspa.wwwnl1-ss2.a2hosted.com/>

## ssh:

ssh oceanspa@nl1-ss2.a2hosting.com  -p 7822


Notes to self:

`cd ~/git/oceansPastInitiative/new_website/site/ && php -S 127.0.0.1:7000` — runs a local php server for templating

Currently using loads of js, will concatinate to limit http requests:
- js/jquery-3.3.1.min.js
- js/jquery-ui.min.js
- js/fastclick.js
- js/scroll.js
- js/fixed-responsive-nav.js
- js/toc.min.js

<!-- # using composer to manage php dependencies:

https://github.com/delight-im/Knowledge/blob/master/Composer%20(PHP).md

https://getcomposer.org/download/

* Open the official download page and execute the four commands shown at the top. This needs the php executable somewhere in your PATH. Otherwise, prepend the full path to that executable on your machine.
* Run the following command to make Composer available globally as composer: `$ sudo mv composer.phar /usr/local/bin/composer` -->

# Local MariaDB notes :

Once installed (I followed this: https://www.unixmen.com/install-mariadb-arch-linuxmanjaro/) run

`$ systemctl status mariadb`

depending on  answer:

`$ sudo systemctl start mariadb`

and / or

`$ mysql -u root -p`

then enter password

# using PHP-Login :
